package com.os.bbh.app;

import com.os.bbh.app.model.Agency;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AgencyManager {
    public static Map<String, Agency> agencyBySessionTicketMap = new ConcurrentHashMap<>();
    public static Map<Integer, Agency> agencyByIdMap = new ConcurrentHashMap<>();

    public static void addAgency(Agency agency) {
        //check remove agency cu
        if (agencyByIdMap.containsKey(agency.appAgency.getId())) {
            Agency old = agencyByIdMap.get(agency.appAgency.getId());
            removeAgency(old);
        }

        agencyBySessionTicketMap.put(agency.sessionTicket, agency);
        agencyByIdMap.put(agency.appAgency.getId(), agency);
    }

    public static Agency getAgencyBySessionTicket(String sessionTicket) {
        if (sessionTicket == null) return null;
        return agencyBySessionTicketMap.get(sessionTicket);
    }

    public static Agency getAgencyById(int agencyId) {
        return agencyByIdMap.get(agencyId);
    }

    public static void removeAgency(Agency agency) {
        if (agency == null) return;
        agencyByIdMap.remove(agency.appAgency.getId());
        agencyBySessionTicketMap.remove(agency.sessionTicket);
    }
}
