package com.os.bbh.app;

import com.os.bbh.common.Config;
import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.*;
import org.hibernate.Session;

import java.sql.Timestamp;
import java.util.List;

public class CacheManager {
    public static AppAgency getAgency(Session session, String username, String password) {
        List<AppAgency> list = session.createQuery("from AppAgency where username=:username and password=:password")
                .setParameter("username", username).setParameter("password", password).setMaxResults(1).list();
        if (list != null && list.size() > 0) return list.get(0);
        return null;
    }

    public static AppAgency getAgency(Session session, String username) {
        List<AppAgency> list = session.createQuery("from AppAgency where username=:username")
                .setParameter("username", username).setMaxResults(1).list();
        if (list != null && list.size() > 0) return list.get(0);
        return null;
    }

    public static AppAgency getAgencyById(Session session, int agencyId) {
        List<AppAgency> list = session.createQuery("from AppAgency where id=:agencyId")
                .setParameter("agencyId", agencyId).setMaxResults(1).list();
        if (list != null && list.size() > 0) return list.get(0);
        return null;
    }

    public static AppAgency createAgency(Session session, String username, String password, String phone, String name, String address, int parentId) {
        if (phone != null && phone.length() > 0) phone = Utils.formatPhone("84", phone);
        AppAgency appAgency = new AppAgency();
        appAgency.setParentId(parentId);
        appAgency.setUsername(username);
        appAgency.setPhone(phone);
        appAgency.setName(name);
        appAgency.setAddress(address);
        appAgency.setPassword(password);
        appAgency.setExpired(new Timestamp(System.currentTimeMillis() + Config.adminConfig.getTrialPeriod()));
        appAgency.setCreated(new Timestamp(System.currentTimeMillis()));
        session.save(appAgency);
        return appAgency;
    }

    public static AppCustomer getCustomer(Session session, int customerId) {
        List<AppCustomer> list = session.createQuery("from AppCustomer where id=:customerId")
                .setParameter("customerId", customerId).setMaxResults(1).list();
        if (list != null && list.size() > 0) return list.get(0);
        return null;
    }

    public static AppCustomer createCustomer(Session session, int agencyId, String name, String phone, String address, long birthDay, String cmnd, String hokhau) {
        if (phone != null && phone.length() > 0) phone = Utils.formatPhone("84", phone);
        if (cmnd != null && cmnd.length() > 0) cmnd = cmnd.trim();
        if (hokhau != null && hokhau.length() > 0) hokhau = hokhau.trim();
        AppCustomer appCustomer = new AppCustomer();
        appCustomer.setAgencyId(agencyId);
        appCustomer.setCreateId(agencyId);
        appCustomer.setName(name);
        appCustomer.setPhone(phone);
        appCustomer.setAddress(address);
        appCustomer.setCmnd(cmnd);
        appCustomer.setHokhau(hokhau);
        appCustomer.setBirthDay(new Timestamp(birthDay));
        appCustomer.setCreated(new Timestamp(System.currentTimeMillis()));
        appCustomer.setState(Constant.CustomerState.ACTIVE);
        session.save(appCustomer);
        return appCustomer;
    }

    public static AppCustomer updateCustomer(Session session, int customerId, String name, String phone, String address, long birthDay) {
        if (phone != null && phone.length() > 0) phone = Utils.formatPhone("84", phone);
        AppCustomer appCustomer = getCustomer(session, customerId);
        if (appCustomer != null) {
            appCustomer.setName(name);
            appCustomer.setPhone(phone);
            appCustomer.setAddress(address);
            appCustomer.setBirthDay(new Timestamp(birthDay));
            session.save(appCustomer);
        }
        return appCustomer;
    }

    public static AppAdmin getAdmin(Session session, String username, String password) {
        List<AppAdmin> list = session.createQuery("from AppAdmin where username=:username and password=:password")
                .setParameter("username", username).setParameter("password", password).setMaxResults(1).list();
        if (list != null && list.size() > 0) return list.get(0);
        return null;
    }

    public static AppKey getKey(Session session, String key) {
        List<AppKey> list = session.createQuery("from AppKey where key=:key")
                .setParameter("key", key).setMaxResults(1).list();
        if (list != null && list.size() > 0) return list.get(0);
        return null;
    }

    public static AppKey createKey(Session session, long time, String adminUsername) {
        AppKey appKey = new AppKey();
        appKey.setKey(Utils.randStr(32));
        appKey.setTime(time);
        appKey.setActive(0);
        appKey.setCreateBy(adminUsername);
        appKey.setCreated(new Timestamp(System.currentTimeMillis()));
        session.save(appKey);
        return appKey;
    }

    public static AppVoucher getVoucher(Session session, int voucherId, int agencyId) {
        List<AppVoucher> list = session.createQuery("from AppVoucher where id=:voucherId and agencyId=:agencyId")
                .setParameter("voucherId", voucherId)
                .setParameter("agencyId", agencyId)
                .setMaxResults(1).list();
        if (list != null && list.size() > 0) return list.get(0);
        return null;
    }

    public static AppVoucher getVoucher(Session session, int voucherId) {
        List<AppVoucher> list = session.createQuery("from AppVoucher where id=:voucherId")
                .setParameter("voucherId", voucherId)
                .setMaxResults(1).list();
        if (list != null && list.size() > 0) return list.get(0);
        return null;
    }
}
