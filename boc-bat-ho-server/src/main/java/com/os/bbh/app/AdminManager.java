package com.os.bbh.app;

import com.os.bbh.app.model.Admin;
import com.os.bbh.mysql.AppAdmin;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AdminManager {
    public static Map<String, Admin> adminBySessionTicketMap = new ConcurrentHashMap<>();

    public static void addAdmin(Admin admin) {
        adminBySessionTicketMap.put(admin.sessionTicket, admin);
    }

    public static Admin getAdminBySessionTicket(String sessionTicket) {
        if (sessionTicket == null) return null;
        return adminBySessionTicketMap.get(sessionTicket);
    }

    public static void removeAdmin(String sessionTicket) {
        if (sessionTicket == null) return;
        adminBySessionTicketMap.remove(sessionTicket);
    }
}
