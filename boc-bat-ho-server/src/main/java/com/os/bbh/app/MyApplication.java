package com.os.bbh.app;

import com.os.bbh.common.CheckCollectDebtThread;
import com.os.bbh.common.Config;
import com.os.bbh.common.Utils;
import com.os.bbh.rest.admin.AdminChangePassService;
import com.os.bbh.rest.admin.AdminCreateKeyService;
import com.os.bbh.rest.admin.AdminLoginService;
import com.os.bbh.rest.admin.AdminLogoutService;
import com.os.bbh.rest.agency.*;
import com.os.bbh.rest.common.CustomerAddImageService;
import com.os.bbh.rest.common.CustomerDeleteImageService;
import com.os.bbh.rest.common.ViewImageService;
import com.os.bbh.rest.customer.*;
import com.os.bbh.rest.dashboard.AgencyGetDashBoard2DetailService;
import com.os.bbh.rest.dashboard.AgencyGetDashBoard2Service;
import com.os.bbh.rest.dashboard.AgencyGetDashBoardService;
import com.os.bbh.rest.notify.AgencyGetListNotifyService;
import com.os.bbh.rest.notify.AgencyGetUnreadNotifyService;
import com.os.bbh.rest.notify.AgencyReadNotifyService;
import com.os.bbh.rest.paymoney.*;
import com.os.bbh.rest.topup.AgencyCreateTopupService;
import com.os.bbh.rest.transfermoney.AgencyActionTransferMoneyService;
import com.os.bbh.rest.transfermoney.AgencyCreateTransferMoneyService;
import com.os.bbh.rest.transfermoney.AgencyDeleteTransferMoneyService;
import com.os.bbh.rest.transfermoney.AgencyGetListTransferMoneyService;
import com.os.bbh.rest.voucher.*;

import javax.ws.rs.core.Application;

import java.util.HashSet;
import java.util.Set;

public class MyApplication extends Application {
    private Set<Object> singletons = new HashSet<>();

    public MyApplication() {
        Config.init();

//        DailyHandler dailyHandler = new DailyHandler();
//        dailyHandler.start();

        //admin
        singletons.add(new AdminLoginService());
        singletons.add(new AdminLogoutService());
        singletons.add(new AdminChangePassService());
        singletons.add(new AdminCreateKeyService());
        singletons.add(new AgencyAddKeyService());

        //agency
        singletons.add(new AgencyLoginService());
        singletons.add(new AgencyLogoutService());
        singletons.add(new AgencyRegisterService());
        singletons.add(new AgencyChangePassService());
        singletons.add(new AgencyAddChildService());
        singletons.add(new AgencyCollectDebtChildService());
        singletons.add(new AgencyGetListChildService());
        singletons.add(new AgencyRegisterGcmService());
        singletons.add(new AgencyGetInfoService());

        //customer
        singletons.add(new AgencyAddCustomerService());
        singletons.add(new AgencyEditCustomerService());
        singletons.add(new AgencyGetListCustomerService());
        singletons.add(new AgencySearchCustomerService());
        singletons.add(new AgencyPauseCustomerService());
        singletons.add(new AgencyShareCustomerService());
        singletons.add(new AgencyCheckBadDebtService());
        singletons.add(new CustomerAddImageService());
        singletons.add(new CustomerDeleteImageService());

        //voucher
        singletons.add(new AgencyCreateVoucherService());
        singletons.add(new AgencyEditVoucherService());
        singletons.add(new AgencyGetListVoucherService());
        singletons.add(new AgencyActionVoucherService());
        singletons.add(new AgencyCompleteVoucherService());
        singletons.add(new AgencyDeleteVoucherService());
        singletons.add(new AgencyGetHistoryVoucherService());

        //paymoney
        singletons.add(new AgencyCreatePayMoneyService());
        singletons.add(new AgencyDeletePayMoneyService());
        singletons.add(new AgencyEditPayMoneyService());
        singletons.add(new AgencyPaidPayMoneyService());
        singletons.add(new AgencyGetListPayMoneyService());

        //transfer money
        singletons.add(new AgencyCreateTransferMoneyService());
        singletons.add(new AgencyGetListTransferMoneyService());
        singletons.add(new AgencyDeleteTransferMoneyService());
        singletons.add(new AgencyActionTransferMoneyService());

        //notify
        singletons.add(new AgencyGetListNotifyService());
        singletons.add(new AgencyGetUnreadNotifyService());
        singletons.add(new AgencyReadNotifyService());

        //topup
        singletons.add(new AgencyCreateTopupService());

        //dashboard
        singletons.add(new AgencyGetDashBoardService());
        singletons.add(new AgencyGetDashBoard2Service());
        singletons.add(new AgencyGetDashBoard2DetailService());
        singletons.add(new ViewImageService());

        CheckCollectDebtThread.init();
        Utils.log("TAG", "Init finish");
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
