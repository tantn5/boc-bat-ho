package com.os.bbh.mysql;

import com.os.bbh.common.Utils;
import com.os.bbh.rest.agency.response.AgencyInfo;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "app_agency")
public class AppAgency {
    private int id;
    private int parentId;//agencyParent
    private String username;
    private String password;
    private String name;
    private String nameAscii;
    private String phone;
    private String address;
    private double money;//vốn
    private Timestamp expired;
    private Timestamp created;

    @Transient
    public AgencyInfo getAgencyInfo() {
        AgencyInfo agencyInfo = new AgencyInfo();
        agencyInfo.id = id;
        agencyInfo.parentId = parentId;
        agencyInfo.username = username;
        agencyInfo.phone = phone;
        agencyInfo.name = name;
        agencyInfo.address = address;
        agencyInfo.money = money;
        agencyInfo.expired = expired;
        agencyInfo.created = created;
        return agencyInfo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "parent_id")
    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.nameAscii = Utils.getTextSearch(name);
    }

    @Column(name = "name_ascii")
    public String getNameAscii() {
        return nameAscii;
    }

    public void setNameAscii(String nameAscii) {
        this.nameAscii = nameAscii;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "money")
    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    @Column(name = "expired")
    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Transient
    public boolean expired() {
        if (expired == null) return true;
        return System.currentTimeMillis() > expired.getTime();
    }

    @Transient
    public synchronized boolean enough(double money) {
        return this.money >= money && money > 0;
    }

    @Transient
    public synchronized void updateMoney(double dMoney) {
        setMoney(this.money + dMoney);
    }
}
