package com.os.bbh.mysql;

import com.os.bbh.rest.notify.response.NotifyInfo;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.hibernate.Session;

import javax.persistence.*;
import java.io.IOException;
import java.sql.Timestamp;

@Entity
@Table(name = "app_notify")
public class AppNotify {
    private int id;
    private int ownerId;
    private int parentId;
    private int childId;
    private int customerId;
    private int voucherId;
    private int transferMoneyId;
    private String des;
    private int type;
    private int unread = 1;
    private Timestamp created = new Timestamp(System.currentTimeMillis());

    @Transient
    public NotifyInfo getNotifyInfo(Session session) {
        NotifyInfo notifyInfo = new NotifyInfo();
        notifyInfo.notifyId = id;
        notifyInfo.ownerId = ownerId;
        notifyInfo.parentId = parentId;
        notifyInfo.childId = childId;
        notifyInfo.customerId = customerId;
        notifyInfo.voucherId = voucherId;
        notifyInfo.transferMoneyId = transferMoneyId;
        notifyInfo.des = des;
        notifyInfo.type = type;
        notifyInfo.read = 1 - unread;
        notifyInfo.created = created.getTime();
        return notifyInfo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "parent_id")
    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    @Column(name = "owner_id")
    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    @Column(name = "child_id")
    public int getChildId() {
        return childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    @Column(name = "customer_id")
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Column(name = "voucher_id")
    public int getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(int voucherId) {
        this.voucherId = voucherId;
    }

    @Column(name = "transfer_money_id")
    public int getTransferMoneyId() {
        return transferMoneyId;
    }

    public void setTransferMoneyId(int transferMoneyId) {
        this.transferMoneyId = transferMoneyId;
    }

    @Column(name = "des")
    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    @Column(name = "type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Column(name = "unread")
    public int getUnread() {
        return unread;
    }

    public void setUnread(int unread) {
        this.unread = unread;
    }

    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }
}
