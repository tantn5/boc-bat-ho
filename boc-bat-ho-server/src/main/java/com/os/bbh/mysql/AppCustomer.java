package com.os.bbh.mysql;

import com.os.bbh.common.Utils;
import com.os.bbh.rest.customer.response.CustomerInfo;
import com.os.bbh.rest.customer.response.ImageInfo;
import org.hibernate.Session;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "app_customer")
public class AppCustomer {
    private int id;
    private int agencyId;
    private int createId;
    private String name;
    private String nameAscii;
    private String phone;
    private String address;
    private Timestamp birthDay;
    private double totalDebt;
    private double remainDebt;
    private int state;
    private Timestamp created;
    private String cmnd;
    private String hokhau;

    @Transient
    public CustomerInfo getCustomerInfo(Session session) {
        CustomerInfo customerInfo = new CustomerInfo();
        customerInfo.id = id;
        customerInfo.agencyId = agencyId;
        customerInfo.createId = createId;
        customerInfo.phone = phone;
        customerInfo.name = name;
        customerInfo.address = address;
        customerInfo.birthDay = birthDay;
        customerInfo.totalDebt = totalDebt;
        customerInfo.remainDebt = remainDebt;
        customerInfo.created = created;
        customerInfo.state = state;
        customerInfo.cmnd = cmnd;
        customerInfo.hokhau = hokhau;

        List<AppImage> list = session.createQuery("from AppImage where customerId=:customerId").setParameter("customerId", id).list();
        if (list != null) {
            customerInfo.imageInfos = new ArrayList<>();
            for (AppImage appImage : list) {
                customerInfo.imageInfos.add(appImage.getImageInfo());
            }
        }

        return customerInfo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "agency_id")
    public int getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(int agencyId) {
        this.agencyId = agencyId;
    }

    @Column(name = "create_id")
    public int getCreateId() {
        return createId;
    }

    public void setCreateId(int createId) {
        this.createId = createId;
    }

    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.nameAscii = Utils.getTextSearch(name);
    }

    @Column(name = "name_ascii")
    public String getNameAscii() {
        return nameAscii;
    }

    public void setNameAscii(String nameAscii) {
        this.nameAscii = nameAscii;
    }

    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "birth_day")
    public Timestamp getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Timestamp birthDay) {
        this.birthDay = birthDay;
    }

    @Column(name = "total_debt")
    public double getTotalDebt() {
        return totalDebt;
    }

    public void setTotalDebt(double totalDebt) {
        this.totalDebt = totalDebt;
    }

    @Column(name = "remain_debt")
    public double getRemainDebt() {
        return remainDebt;
    }

    public void setRemainDebt(double remainDebt) {
        this.remainDebt = remainDebt;
    }

    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Column(name = "state")
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Column(name = "cmnd")
    public String getCmnd() {
        return cmnd;
    }

    public void setCmnd(String cmnd) {
        this.cmnd = cmnd;
    }

    @Column(name = "hokhau")
    public String getHokhau() {
        return hokhau;
    }

    public void setHokhau(String hokhau) {
        this.hokhau = hokhau;
    }

    @Transient
    public synchronized boolean enough(double debt) {
        return this.remainDebt >= debt && debt > 0;
    }

    @Transient
    public synchronized void updateTotalDebt(double dDebt) {
        setTotalDebt(this.totalDebt + dDebt);
    }

    @Transient
    public synchronized void updateRemainDebt(double dDebt) {
        setRemainDebt(this.remainDebt + dDebt);
    }
}
