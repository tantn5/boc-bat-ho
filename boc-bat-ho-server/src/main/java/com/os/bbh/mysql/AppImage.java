package com.os.bbh.mysql;

import com.os.bbh.rest.customer.response.ImageInfo;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "app_image")
public class AppImage {
    private int id;
    private int customerId;
    private String name;
    private int deleted;
    private Timestamp created;

    @Transient
    public ImageInfo getImageInfo() {
        ImageInfo imageInfo = new ImageInfo();
        imageInfo.url = "http://171.244.50.90:8080/boc_bat_ho_server_war/ViewImage/" + name;
        imageInfo.imageId = id;
        return imageInfo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "customer_id")
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "deleted")
    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }
}
