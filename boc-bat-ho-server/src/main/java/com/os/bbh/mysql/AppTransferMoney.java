package com.os.bbh.mysql;

import com.os.bbh.app.CacheManager;
import com.os.bbh.rest.admin.response.AdminInfo;
import com.os.bbh.rest.transfermoney.response.TransferMoneyInfo;
import org.hibernate.Session;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "app_transfer_money")
public class AppTransferMoney {
    private int id;
    private int parentId;
    private int childId;
    private double money;
    private int state;
    private Timestamp created;

    @Transient
    public TransferMoneyInfo getTransferMoneyInfo(Session session) {
        TransferMoneyInfo transferMoneyInfo = new TransferMoneyInfo();
        transferMoneyInfo.transferMoneyId = id;
        AppAgency parentAppAgency = CacheManager.getAgencyById(session, parentId);
        if (parentAppAgency != null) transferMoneyInfo.parentInfo = parentAppAgency.getAgencyInfo();
        AppAgency childAppAgency = CacheManager.getAgencyById(session, childId);
        if (childAppAgency != null) transferMoneyInfo.childInfo = childAppAgency.getAgencyInfo();
        transferMoneyInfo.state = state;
        transferMoneyInfo.created = created.getTime();
        transferMoneyInfo.money = money;
        return transferMoneyInfo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "parent_id")
    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    @Column(name = "child_id")
    public int getChildId() {
        return childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    @Column(name = "money")
    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    @Column(name = "state")
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }
}
