package com.os.bbh.mysql;

import com.os.bbh.rest.admin.response.AdminInfo;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "app_statistic")
public class AppStatistic {
    private int id;
    private int agencyId;
    private double money;
    private int type;
    private int appPayMoneyId;
    private int appTransferMoneyId;
    private int appVoucherId;
    private Timestamp created = new Timestamp(System.currentTimeMillis());

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "agency_id")
    public int getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(int agencyId) {
        this.agencyId = agencyId;
    }

    @Column(name = "money")
    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    @Column(name = "type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Column(name = "app_pay_money_id")
    public int getAppPayMoneyId() {
        return appPayMoneyId;
    }

    public void setAppPayMoneyId(int appPayMoneyId) {
        this.appPayMoneyId = appPayMoneyId;
    }


    @Column(name = "app_transfer_money_id")
    public int getAppTransferMoneyId() {
        return appTransferMoneyId;
    }

    public void setAppTransferMoneyId(int appTransferMoneyId) {
        this.appTransferMoneyId = appTransferMoneyId;
    }

    @Column(name = "app_voucher_id")
    public int getAppVoucherId() {
        return appVoucherId;
    }

    public void setAppVoucherId(int appVoucherId) {
        this.appVoucherId = appVoucherId;
    }

    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }
}
