package com.os.bbh.mysql;

import com.os.bbh.common.Utils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by Tantn on 3/10/2017.
 */
public class HibernateUtils {
    private static final String TAG = HibernateUtils.class.getSimpleName();

    private static SessionFactory sessionFactory;

    static {
        try {
            sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
            openSession();
        } catch (Throwable ex) {
            Utils.error(TAG, ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    private static Session openSession() throws HibernateException {
        Session session = sessionFactory.openSession();
        return session;
    }

    public static Session getSession() {
        Session session = sessionFactory.getCurrentSession();

        return session;
    }

    public static void close() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
        sessionFactory = null;
    }

}
