package com.os.bbh.mysql;

import com.os.bbh.rest.admin.response.AdminInfo;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "app_admin")
public class AppAdmin {
    private int id;
    private String username;
    private String password;
    private Timestamp created;

    @Transient
    public AdminInfo getAdminInfo() {
        AdminInfo adminInfo = new AdminInfo();
        adminInfo.username = username;
        return adminInfo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }
}
