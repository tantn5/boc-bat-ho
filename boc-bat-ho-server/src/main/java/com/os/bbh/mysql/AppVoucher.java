package com.os.bbh.mysql;

import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.rest.voucher.response.VoucherInfo;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "app_voucher")
public class AppVoucher {
    private int id;
    private int type;
    private int agencyId;
    private int customerId;
    private Timestamp startDate;
    private Timestamp endDate;
    private int numDay;//chỉ dùng khi bốc họ
    private int periodThuLai;//chu kì thu lãi - theo ngày
    private double loan;//tiền vay ban đầu
    private double remainLoan;
    private double collectedDebt;//số tiền đã thu được
    private double interestRateLai;//lãi suất vay lãi - 1000/1tr/day
    private float interestRateHo;//lãi suất bốc họ
    private String reference;
    private String referencePhone;
    private String referenceDes;
    private int state = Constant.VoucherState.RUNNING;//trạng thái của chứng từ
    private int action = Constant.VoucherAction.IN_DEBT;
    private int deleted;
    private Timestamp created = new Timestamp(System.currentTimeMillis());

    @Transient
    public VoucherInfo getVoucherInfo() {
        VoucherInfo voucherInfo = new VoucherInfo();
        voucherInfo.voucherId = id;
        voucherInfo.type = type;
        voucherInfo.agencyId = agencyId;
        voucherInfo.customerId = customerId;
        if (startDate != null) voucherInfo.startDate = startDate.getTime();
        voucherInfo.numDay = numDay;
        voucherInfo.endDate = getTimeEndDate();
        voucherInfo.periodThuLai = periodThuLai;
        voucherInfo.loan = loan;
        voucherInfo.remainLoan = remainLoan;
        voucherInfo.interestRateLai = interestRateLai;
        voucherInfo.interestRateHo = interestRateHo;
        voucherInfo.reference = reference;
        voucherInfo.referencePhone = referencePhone;
        voucherInfo.referenceDes = referenceDes;
        voucherInfo.state = state;
        voucherInfo.action = action;
        if (created != null) voucherInfo.created = created.getTime();
        voucherInfo.nextCollectDay = getNextCollectTime();
        voucherInfo.nextCollectMoney = getNextCollectMoney();
        return voucherInfo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Column(name = "agency_id")
    public int getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(int agencyId) {
        this.agencyId = agencyId;
    }

    @Column(name = "customer_id")
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Column(name = "start_date")
    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    @Column(name = "end_date")
    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    @Transient
    public long getTimeEndDate() {
        if (endDate != null) {
            return endDate.getTime();
        } else if (numDay > 0) {
            return startDate.getTime() + Utils.dayToMillisecond(numDay);
        }
        return 0;
    }

    @Column(name = "num_day")
    public int getNumDay() {
        return numDay;
    }

    public void setNumDay(int numDay) {
        this.numDay = numDay;
    }

    @Column(name = "period_thu_lai")
    public int getPeriodThuLai() {
        return periodThuLai;
    }

    public void setPeriodThuLai(int periodThuLai) {
        this.periodThuLai = periodThuLai;
    }

    @Column(name = "loan")
    public double getLoan() {
        return loan;
    }

    public void setLoan(double loan) {
        this.loan = loan;
    }

    @Column(name = "interest_rate_lai")
    public double getInterestRateLai() {
        return interestRateLai;
    }

    public void setInterestRateLai(double interestRateLai) {
        this.interestRateLai = interestRateLai;
    }

    @Column(name = "interest_rate_ho")
    public float getInterestRateHo() {
        return interestRateHo;
    }

    public void setInterestRateHo(float interestRateHo) {
        this.interestRateHo = interestRateHo;
    }

    @Transient
    public double getInterestRatePerday() {
        if (type == Constant.VoucherType.LAI) {
            return (loan / 1000000) * interestRateLai;
        } else if (type == Constant.VoucherType.HO) {
            return loan / numDay;
        }
        return 0;
    }

    @Column(name = "reference")
    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Column(name = "reference_phone")
    public String getReferencePhone() {
        return referencePhone;
    }

    public void setReferencePhone(String referencePhone) {
        this.referencePhone = referencePhone;
    }

    @Column(name = "reference_des")
    public String getReferenceDes() {
        return referenceDes;
    }

    public void setReferenceDes(String referenceDes) {
        this.referenceDes = referenceDes;
    }

    @Column(name = "state")
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Column(name = "action")
    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Column(name = "remain_loan")
    public double getRemainLoan() {
        return remainLoan;
    }

    public void setRemainLoan(double remainLoan) {
        this.remainLoan = remainLoan;
    }

    @Column(name = "collected_debt")
    public double getCollectedDebt() {
        return collectedDebt;
    }

    public void setCollectedDebt(double collectedDebt) {
        this.collectedDebt = collectedDebt;
    }

    @Column(name = "deleted")
    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @Transient
    public void collectDebt(double money) {
        collectedDebt += money;
        if (type == Constant.VoucherType.LAI) {
            //if (money == remainLoan) remainLoan = 0;
        } else if (type == Constant.VoucherType.HO) {
            remainLoan -= money;
        }
    }

    @Transient
    public MoneyDay getNextCollect() {
        long dTime = Utils.getTimeInLastDay() - startDate.getTime();
        int day = (int) (dTime / Utils.timePerDay() + 1);//số ngày đã vay lãi - bao gồm cả ngày đầu tiên mới vay
        List<MoneyDay> listMoneyDay = getListMoneyDay();
//        System.out.println("day=" + day);
        //System.out.println("listMoneyDay=" + listMoneyDay);
        int nextCollectDay = 0;
        double nextCollectMoney = 0;
        for (MoneyDay moneyDay : listMoneyDay) {
            if (nextCollectDay == 0) {
                if (moneyDay.day >= day || listMoneyDay.indexOf(moneyDay) == listMoneyDay.size() - 1) {
//                    System.out.println("getNextCollect: moneyDay=" + moneyDay);
                    if (collectedDebt < moneyDay.money) {
                        nextCollectDay = moneyDay.day;
                        nextCollectMoney = moneyDay.money - collectedDebt;
                        break;
                    }
                }
            }
//            System.out.println("nextCollectDay" + nextCollectDay);
        }

        //System.out.println("nextCollectDay=" + nextCollectDay);
        //System.out.println("nextCollectMoney=" + nextCollectMoney);
        return new MoneyDay(nextCollectDay, nextCollectMoney);
    }

    @Transient
    public List<MoneyDay> getListMoneyDay() {
        List<MoneyDay> listMoneyDay = new ArrayList<>();//list ngày thu và số tiền thu tương ứng
        int dDay = 1;
        while (true) {
            int nextDay = dDay * periodThuLai;
            if (nextDay >= numDay) {
                if (listMoneyDay.size() == 0 || listMoneyDay.get(listMoneyDay.size() - 1).day != numDay) {
                    listMoneyDay.add(new MoneyDay(numDay, getMoneyInDay(numDay)));
                }
                break;
            }
            listMoneyDay.add(new MoneyDay(nextDay, getMoneyInDay(nextDay)));
            dDay++;
        }
        return listMoneyDay;
    }

    @Transient
    public double getMoneyInDay(int day) {//get tổng số tiền có thể thu được tính đến ngày này
        double money = 0;
        if (type == Constant.VoucherType.LAI) {
            double collectPerDay = getCollectPerDay();
            money = collectPerDay * day;
            if (day >= numDay) money += loan;
        } else if (type == Constant.VoucherType.HO) {
            double collectPerDay = getCollectPerDay();
            money = collectPerDay * day;
        }
        return money;
    }

    @Transient
    public double getCollectPerDay() {
        if (type == Constant.VoucherType.LAI) return loan / 1000000f * interestRateLai;
        else if (type == Constant.VoucherType.HO) return loan / numDay;
        return 0;
    }

    @Transient
    public long getNextCollectTime() {
        MoneyDay moneyDay = getNextCollect();
        //System.out.println("getNextCollectTime: moneyDay=" + moneyDay);
        return (moneyDay.day - 1) * Utils.timePerDay() + startDate.getTime();
    }

    public boolean needCollectToday() {
        long nextCollectTime = getNextCollectTime();
        return Utils.isSameDay(new Date(nextCollectTime), new Date());
    }

    @Transient
    public double getNextCollectMoney() {
        MoneyDay moneyDay = getNextCollect();
        return moneyDay.money;
    }

    public static class MoneyDay {
        public int day;
        public double money;

        public MoneyDay(int day, double money) {
            this.day = day;
            this.money = money;
        }

        @Override
        public String toString() {
            return "\nday=" + day + ", money=" + money;
        }
    }

    public static void main(String[] args) {
        AppVoucher appVoucher = new AppVoucher();
        appVoucher.type = Constant.VoucherType.LAI;
        appVoucher.startDate = new Timestamp(1567162680000L);
        appVoucher.numDay = 50;
        appVoucher.periodThuLai = 1;
        appVoucher.loan = 10000000;
        appVoucher.collectedDebt = 0;
        appVoucher.remainLoan = 1000000;
        appVoucher.interestRateLai = 2000;
        appVoucher.interestRateHo = 0.8f;
        System.out.println(appVoucher.getNextCollectMoney());
        System.out.println(new Date(appVoucher.getNextCollectTime()));

        System.out.println("test1: " + new Date(1567162680000L));
        System.out.println("test2: " + new Date(Utils.getTimeInLastDay()));

    }
//    public static void main(String[]args){
//        Timestamp timestamp = new Timestamp(1559001600000L);
//        System.out.println(timestamp);
//        System.out.println(Utils.getTimeInLastDay());
//    }
}
