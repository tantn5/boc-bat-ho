package com.os.bbh.mysql;

import com.os.bbh.rest.admin.response.KeyInfo;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "app_key")
public class AppKey {
    private int id;
    private String key;
    private long time;
    private int active;
    private int agencyId;//agency active key nay
    private String createBy;
    private Timestamp useDate;
    private Timestamp created;

    @Transient
    public KeyInfo getKeyInfo() {
        KeyInfo keyInfo = new KeyInfo();
        keyInfo.key = key;
        keyInfo.time = time;
        keyInfo.active = active;
        keyInfo.agencyId = agencyId;
        keyInfo.createBy = createBy;
        if (useDate != null) keyInfo.useDate = useDate.getTime();
        if (created != null) keyInfo.created = created.getTime();
        return keyInfo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "_key")
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Column(name = "time")
    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Column(name = "active")
    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    @Column(name = "agency_id")
    public int getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(int agencyId) {
        this.agencyId = agencyId;
    }

    @Column(name = "create_by")
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Column(name = "use_date")
    public Timestamp getUseDate() {
        return useDate;
    }

    public void setUseDate(Timestamp useDate) {
        this.useDate = useDate;
    }

    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }
}
