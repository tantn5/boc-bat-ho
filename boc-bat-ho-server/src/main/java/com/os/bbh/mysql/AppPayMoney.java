package com.os.bbh.mysql;

import com.os.bbh.rest.admin.response.AdminInfo;
import com.os.bbh.rest.paymoney.response.PayMoneyInfo;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "app_pay_money")
public class AppPayMoney {
    private int id;
    private int agencyId;
    private double money;
    private String purpose;
    private Timestamp payDate;
    private int type;
    private int deleted;
    private int paid;//đã chi tiền
    private Timestamp created;

    @Transient
    public PayMoneyInfo getPayMoneyInfo() {
        PayMoneyInfo payMoneyInfo = new PayMoneyInfo();
        payMoneyInfo.payMoneyId = id;
        payMoneyInfo.agencyId = agencyId;
        payMoneyInfo.money = money;
        payMoneyInfo.purpose = purpose;
        if (payDate != null) payMoneyInfo.payDate = payDate.getTime();
        payMoneyInfo.type = type;
        payMoneyInfo.paid = paid;
        if (created != null) payMoneyInfo.created = created.getTime();
        return payMoneyInfo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "agency_id")
    public int getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(int agencyId) {
        this.agencyId = agencyId;
    }

    @Column(name = "money")
    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    @Column(name = "purpose")
    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    @Column(name = "pay_date")
    public Timestamp getPayDate() {
        return payDate;
    }

    public void setPayDate(Timestamp payDate) {
        this.payDate = payDate;
    }

    @Column(name = "type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Column(name = "deleted")
    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @Column(name = "paid")
    public int getPaid() {
        return paid;
    }

    public void setPaid(int paid) {
        this.paid = paid;
    }

    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }
}
