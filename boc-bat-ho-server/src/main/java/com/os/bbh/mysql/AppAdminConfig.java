package com.os.bbh.mysql;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "app_admin_config")
public class AppAdminConfig {
    private int id;
    private long trialPeriod;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "trial_period")
    public long getTrialPeriod() {
        return trialPeriod;
    }

    public void setTrialPeriod(long trialPeriod) {
        this.trialPeriod = trialPeriod;
    }
}
