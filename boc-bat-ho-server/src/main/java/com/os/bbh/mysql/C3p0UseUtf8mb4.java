package com.os.bbh.mysql;

import com.mchange.v2.c3p0.AbstractConnectionCustomizer;

import java.sql.Connection;
import java.sql.Statement;

public class C3p0UseUtf8mb4 extends AbstractConnectionCustomizer {
    @Override
    public void onAcquire(Connection c, String parentDataSourceIdentityToken)
            throws Exception {
        super.onAcquire(c, parentDataSourceIdentityToken);
        try(Statement stmt = c.createStatement()) {
            stmt.executeQuery("SET NAMES utf8mb4");
        }
    }
}