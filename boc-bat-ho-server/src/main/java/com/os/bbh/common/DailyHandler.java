package com.os.bbh.common;

import com.os.bbh.mysql.AppVoucher;
import com.os.bbh.mysql.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class DailyHandler implements Runnable {
    private static final String TAG = DailyHandler.class.getSimpleName();

    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);

    public void start() {
        Date date = new Date();
        date.setDate(date.getDate() + 1);
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(1);
        scheduledExecutorService.schedule(this, (date.getTime() - System.currentTimeMillis()) / 1000, TimeUnit.SECONDS);
    }

    @Override
    public void run() {
        Session session = HibernateUtils.getSession();//open session
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            //quét xử lý tất cả số nợ ngày mới
            List<AppVoucher> list = session.createQuery("from AppVoucher where deleted=0")
                    .list();
            for (AppVoucher appVoucher : list) {
                if (appVoucher.getType() == Constant.VoucherType.LAI) {

                } else if (appVoucher.getType() == Constant.VoucherType.HO) {
                    
                }
            }

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            Utils.error(TAG, e);
        }
        start();
    }
}
