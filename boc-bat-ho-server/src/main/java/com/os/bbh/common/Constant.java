package com.os.bbh.common;

public class Constant {
    public static final String APP_NAME = "Clever Money";
    public static final int NAME_LENGTH_MIN = 5;
    public static final int NAME_LENGTH_MAX = 32;
    public static final int USERNAME_LENGTH_MIN = 5;
    public static final int USERNAME_LENGTH_MAX = 32;
    public static final int PASS_LENGTH_MIN = 5;
    public static final int PASS_LENGTH_MAX = 32;
    public static final int DES_LENGTH_MIN = 0;
    public static final int DES_LENGTH_MAX = 255;

    public static class PlatformType {
        public static final int IOS = 0;
        public static final int ANDROID = 1;
    }

    public static class VoucherType {
        public static final int LAI = 0;
        public static final int HO = 1;
    }

    public static class VoucherState {
        public static final int RUNNING = 0;
        public static final int COMPLETE = 1;
        public static final int PAUSE = 2;
    }

    public static class VoucherAction {
        public static final int COLLECTED_DEBT = 0;
        public static final int IN_DEBT = 1;
        public static final int COLLECTING = 2;
        public static final int WAITING = 3;
    }

    public static class PayMoneyType {
        public static final int TIEN_MAT = 0;
        public static final int CHUYEN_KHOAN = 1;
    }

    public static class CustomerState {
        public static final int ACTIVE = 0;
        public static final int COMPLETE = 1;
        public static final int PAUSE = 2;
        public static final int ALL = 3;
    }

    public static class TransferMoneyState {
        public static final int WAITING = 0;
        public static final int ACCEPT = 1;
        public static final int REJECT = 2;
        public static final int ALL = 3;
    }

    public static class NotifyType {
        public static final int CHILD_COLLECTED_DEBT_CUSTOMER = 0;
        public static final int PARENT_TRANSFER_MONEY_TO_CHILD = 1;
        public static final int PARENT_PAUSE_CUSTOMER = 2;
        public static final int PARENT_COMPLETE_VOUCHER = 3;
        public static final int CHILD_COMPLETE_VOUCHER = 4;
        public static final int PARENT_COLLECT_DEBT_CHILD = 5;
        public static final int NOTIFY_COLLECT_DEBT = 6;
    }

    public static class StatisticType{
        public static final int TOPUP_MONEY = 0;
        public static final int CHILD_GET_TRANSFER_FROM_PARENT = 1;
        public static final int PARENT_TRANSFER_TO_CHILD = 2;
        public static final int COLLECT_DEBT = 3;
        public static final int AGENCY_PAID_PAY_MONEY = 4;
        public static final int CREATE_VOUCHER = 5;
    }
}
