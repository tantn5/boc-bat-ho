package com.os.bbh.common;

import java.util.ArrayList;
import java.util.List;

public class ManyThings {
    public List<Thing> things = new ArrayList<Thing>();

    public ManyThings(String values) {
        for (String value : values.split(",")) {
            things.add(new Thing(value));
        }
    }
}
