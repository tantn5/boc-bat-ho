package com.os.bbh.common;

import com.os.bbh.mysql.AppNotify;
import com.os.bbh.mysql.AppRegisterGcm;
import org.hibernate.Session;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Tantn on 6/2/2017.
 */
public class GcmUtils {
    private static final String TAG = GcmUtils.class.getSimpleName();

    private static String AUTHORIZATION_KEY = "key=AIzaSyC3KZwgHL6t1N1gjq65qIiPZNM0gdaXZ8Y";
    private static String URL = "https://fcm.googleapis.com/fcm/send";

    private static boolean sendRequest(String data) {
        HttpsURLConnection connection = null;
        DataOutputStream dataOutputStream = null;
        BufferedWriter writer = null;
        BufferedReader bufferedReader = null;
        try {
            connection = (HttpsURLConnection) new URL(URL).openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setConnectTimeout(3000);
            connection.setRequestProperty("Authorization", AUTHORIZATION_KEY);
            connection.setRequestProperty("content-type", "application/json");
            connection.setRequestProperty("Accept-Charset", "UTF-8");

            dataOutputStream = new DataOutputStream(connection.getOutputStream());
            writer = new BufferedWriter(new OutputStreamWriter(dataOutputStream, "UTF-8"));
            writer.write(data);

            writer.flush();
            writer.close();

            dataOutputStream.flush();
            dataOutputStream.close();

            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            StringBuilder responseBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                responseBuilder.append(line);
            }
            Utils.log(TAG, "url=" + URL);
            Utils.log(TAG, "request=" + data);
            Utils.log(TAG, "response=" + responseBuilder.toString());
            return true;
        } catch (Exception e) {
            Utils.error(TAG, e);
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (Exception e) {
                    Utils.error(TAG, e);
                }
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return false;
    }

    public static void sendNotificationToRegId(int platformType, List<String> regIds, String title, String message) throws Exception {
        Map notificationMap = new HashMap();
        Map dataMap = new HashMap();

        if (platformType == Constant.PlatformType.IOS) {
            notificationMap.put("body", title);
            notificationMap.put("sound", "default");
        }
        notificationMap.put("priority", "high");
        notificationMap.put("vibrate", 300);
        notificationMap.put("lights", true);
        notificationMap.put("title", Constant.APP_NAME);
        dataMap.put("priority", "high");
        dataMap.put("vibrate", 300);
        dataMap.put("lights", true);
        dataMap.put("title", Constant.APP_NAME);
        dataMap.put("message", title);
        JSONObject object = new JSONObject();
        object.put("registration_ids", regIds);
        object.put("time_to_live", 300);
        object.put("data", dataMap);
        object.put("notification", notificationMap);
        sendRequest(object.toString());
    }

    public static void sendNotification(Session session, int agencyId, String title, String message) throws Exception {
        List<AppRegisterGcm> list = session.createQuery("from AppRegisterGcm where agencyId=:agencyId")
                .setParameter("agencyId", agencyId)
                .list();
        List<String> regIds = new ArrayList<>();
        if (list != null && list.size() > 0) {
            for (AppRegisterGcm appRegisterGcm : list) {
                regIds.add(appRegisterGcm.getRegId());
            }
            sendNotificationToRegId(Constant.PlatformType.IOS, regIds, title, message);
        }
    }

    public static void main(String[] args) throws Exception {
        String regId = "c2OugRX-qMk:APA91bE_Ufhw1b1JOUsZOYxDwvD7v6SSPpOngQqkOoDS8kxxYCMzFnAeZbasqBKiGciI5GzjN34LSmj7TNMCp8hfQWUTP1ycZZuZlxCnKjrTiwPK2BkpZc_oPZLjYDYfusDiNhVWjtDA";
        List<String> regIds = new ArrayList<>();
        regIds.add(regId);
        AppNotify appNotify = new AppNotify();
        appNotify.setDes("Test day nhe");
        sendNotificationToRegId(Constant.PlatformType.IOS, regIds, "A đã chuyển 1.000.000 cho B", Utils.toString(appNotify));
    }
}
