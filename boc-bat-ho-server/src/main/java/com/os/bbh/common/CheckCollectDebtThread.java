package com.os.bbh.common;

import com.os.bbh.app.CacheManager;
import com.os.bbh.mysql.*;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

public class CheckCollectDebtThread {
    private static final String TAG = CheckCollectDebtThread.class.getSimpleName();

    public static void init() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(30 * 60 * 1000);

                        Calendar calendar = Calendar.getInstance();
                        if (calendar.get(Calendar.HOUR) == 8 && calendar.get(Calendar.MINUTE) > 0 && calendar.get(Calendar.MINUTE) < 31) {
                            Session session = HibernateUtils.getSession();//open session
                            Transaction transaction = null;
                            try {
                                transaction = session.beginTransaction();

                                List<AppVoucher> vouchers = session.createQuery("from Appvoucher where state=:state")
                                        .setParameter("state", Constant.VoucherState.RUNNING).list();
                                for (AppVoucher appVoucher : vouchers) {
                                    if (appVoucher.needCollectToday()) {
                                        AppCustomer appCustomer = CacheManager.getCustomer(session, appVoucher.getCustomerId());
                                        AppAgency appAgency = CacheManager.getAgencyById(session, appVoucher.getAgencyId());

                                        if (appAgency != null && appCustomer != null) {
                                            //create notify
                                            AppNotify appNotify = new AppNotify();
                                            appNotify.setOwnerId(appAgency.getParentId());
                                            appNotify.setVoucherId(appVoucher.getId());
                                            appNotify.setType(Constant.NotifyType.NOTIFY_COLLECT_DEBT);
                                            appNotify.setDes(String.format("Chứng từ [vay %s] - Thu %s của KH %s", Utils.formatNumber(appVoucher.getLoan()), Utils.formatNumber(appVoucher.getNextCollectMoney()), appCustomer.getName()));
                                            appNotify.setCreated(new Timestamp(System.currentTimeMillis()));
                                            session.save(appNotify);

                                            GcmUtils.sendNotification(session, appAgency.getId(), String.format("Thu tiền chứng từ [vay %s]", appVoucher.getLoan()), appNotify.getDes());
                                        }
                                    }
                                }

                                transaction.commit();
                            } catch (Exception e) {
                                if (transaction != null && transaction.isActive()) {
                                    transaction.rollback();
                                }
                                Utils.error(TAG, e);
                            }
                        }
                    } catch (Exception e) {
                        Utils.error(TAG, e);
                    }
                }
            }
        }).start();
    }
}
