package com.os.bbh.common;

import com.os.bbh.app.CacheManager;
import com.os.bbh.mysql.AppAgency;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.hibernate.Session;
import org.jboss.resteasy.logging.Logger;

import java.io.IOException;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.util.*;
import java.util.regex.Pattern;

public class Utils {
    private static final String TAG = Utils.class.getSimpleName();

    private static final Logger logger = Logger.getLogger(Utils.class);

//    static {
//        org.apache.log4j.PropertyConfigurator.configure(Config.LOG_CONF_PATH);
//    }

    public static void log(String tag, String msg) {
        logger.info(tag + ": " + msg);
        System.out.println(tag + ": " + msg);
    }

    public static void error(String tag, Exception ex) {
        System.out.println(tag + ": error: " + ex.getMessage());
        ex.printStackTrace();
        logger.warn(tag, ex);
    }

    public static void error(String tag, Throwable ex) {
        System.out.println(tag + ": error: " + ex.getMessage());
        ex.printStackTrace();
        logger.warn(tag, ex);
    }

    private static Pattern patternName = Pattern.compile("^[A-Za-z0-9_ ]{" + Constant.NAME_LENGTH_MIN + "," + Constant.NAME_LENGTH_MAX + "}$");
    private static Pattern patternUserName = Pattern.compile("^[A-Za-z0-9_]{" + Constant.USERNAME_LENGTH_MIN + "," + Constant.USERNAME_LENGTH_MAX + "}$");
    private static Pattern patternPass = Pattern.compile("^[A-Za-z0-9_ ]{" + Constant.PASS_LENGTH_MIN + "," + Constant.PASS_LENGTH_MAX + "}$");
    private static Pattern patternDes = Pattern.compile("^[A-Za-z0-9_ ]{" + Constant.DES_LENGTH_MIN + "," + Constant.DES_LENGTH_MAX + "}$");

    public static boolean isValidName(String name) {
        if (name == null || name.trim().length() == 0) return false;
        //return patternName.matcher(name.trim()).find();
        return name.length() >= Constant.NAME_LENGTH_MIN && name.length() <= Constant.NAME_LENGTH_MAX;
    }

    public static boolean isValidDes(String des) {
        if (des == null || des.trim().length() == 0) return false;
//        return patternDes.matcher(des.trim()).find();
        return des.length() >= Constant.DES_LENGTH_MIN && des.length() <= Constant.DES_LENGTH_MAX;
    }

    public static boolean isValidPhone(String phone) {
        if (phone == null || phone.trim().length() == 0) return false;
        phone = phone.replace("+", phone).trim();
        return phone.length() > 5 && StringUtils.isNumeric(phone);
    }

    public static boolean isValidPassword(String password) {
        if (password == null || password.trim().length() == 0) return false;
        return patternPass.matcher(password.trim()).find();
    }

    public static boolean isValidUsername(String username) {
        if (username == null || username.trim().length() == 0) return false;
        return patternUserName.matcher(username.trim()).find();
    }

    private static Random rand = new Random();

    public static int randInt(int min, int max) {
        return rand.nextInt((max - min) + 1) + min;
    }

    public static float randFloat(float min, float max) {
        int minInt = (int) (100 * min);
        int maxInt = (int) (100 * max);
        int randomNum = rand.nextInt((maxInt - minInt) + 1) + minInt;
        return randomNum / 100f;
    }

    public static int randomWithProbably(float[] probablys) {
        float sum = 0;
        for (float probably : probablys) {
            sum += probably;
        }
        float probably = Utils.randFloat(0, sum - 0.1f);
        float currentProbably = 0;
        for (int i = 0; i < probablys.length; i++) {
            if (probably >= currentProbably && probably < currentProbably + probablys[i]) {
                return i;
            }
            currentProbably += probablys[i];
        }
        return 0;
    }

    public static boolean isOccurrence(float probabilityPercent) {
        return Utils.randInt(0, 99) < probabilityPercent;
    }

    private static SecureRandom secureRand = new SecureRandom();

    public static String randStr(int length) {
        return RandomStringUtils.randomAlphanumeric(length);
    }

    public static String removeAccents(String text) {
        return text == null ? null : Normalizer.normalize(text, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }

    public static String removeSpecial(String input) {
        return input.replaceAll("[^a-zA-Z0-9 ]", "");
    }

    public static String getTextSearch(String input) {
        return removeSpecial(CharacterUtils.convertToAscii(input));
    }

    public static String formatNumber(double d) {
        try {
            DecimalFormat format = new DecimalFormat("###,###,###,###,###");
            return format.format(d);
        } catch (Exception e) {
            Utils.error(TAG, e);
        }
        return d + "";
    }

    public static void main(String[] args) {
        System.out.println(formatNumber(3000));
    }

    public static String formatPhone(String dialCode, String phoneNo) {
        String strPhone = "";
        if (phoneNo != null && dialCode != null) {
            if (phoneNo.length() > 7) {
                if (dialCode.startsWith("+")) {
                    dialCode = dialCode.substring(1);
                }
                if (phoneNo.startsWith("00")) {
                    phoneNo = phoneNo.substring(2);// bo so 0 di
                }
                if (phoneNo.startsWith("+")) {
                    phoneNo = phoneNo.substring(1);
                }
                if (phoneNo.startsWith("0")) {
                    phoneNo = phoneNo.substring(1);
                }
                if (phoneNo.startsWith(dialCode)) {
                    phoneNo = phoneNo.substring(dialCode.length(), phoneNo.length());
                }
                phoneNo = dialCode + phoneNo;

                for (int i = 0; i < phoneNo.length(); i++) {
                    char c = phoneNo.charAt(i);
                    if (c >= '0' && c <= '9') {
                        strPhone += c;
                    }
                }
            }
        }
        return strPhone;
    }

    public static long timePerDay() {
        return 24 * 60 * 60 * 1000;
    }

    public static long dayToMillisecond(int numDay) {
        return (long) numDay * timePerDay();
    }

    public static int milliseconToDay(long millisecond) {
        return (int) (millisecond / timePerDay());
    }

    public static long getTimeInLastDay() {//get time cuoi ngay
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timeZone);
        System.out.println(calendar.getTime());
        calendar.set(Calendar.HOUR, 23 - 7);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime().getTime();
    }

    public static boolean checkChildOrIsMe(Session session, int childId, int parentId) {
        boolean handlerAble = false;
        if (parentId == childId) {//đại lý trực tiếp xử lý
            handlerAble = true;
        } else {
            AppAgency appAgency2 = CacheManager.getAgencyById(session, childId);
            if (appAgency2.getParentId() == parentId) {//đại lý mẹ trực tiếp xử lý
                handlerAble = true;
            }
        }
        return handlerAble;
    }

    public static boolean checkParent(Session session, int childId, int parentId) {
        boolean handlerAble = false;
        if (childId == parentId) {
            AppAgency appParentAgency = CacheManager.getAgencyById(session, parentId);
            if (appParentAgency != null && appParentAgency.getParentId() == 0) handlerAble = true;
        } else {
            AppAgency appChildAgency = CacheManager.getAgencyById(session, childId);
            if (appChildAgency != null && appChildAgency.getParentId() == parentId) {//đại lý mẹ trực tiếp xử lý
                handlerAble = true;
            }
        }
        return handlerAble;
    }

    public static String toString(Object ob) {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            return ow.writeValueAsString(ob);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static List<Integer> getListChildId(Session session, int parentId) {
        List<Integer> list = new ArrayList<>();
        List<AppAgency> listChild = session.createQuery("from AppAgency where parentId=:parentId")
                .setParameter("parentId", parentId).list();
        if (listChild != null && listChild.size() > 0) {
            for (AppAgency appAgency : listChild) {
                list.add(appAgency.getId());
            }
        }
        return list;
    }

    public static boolean isSameDay(Date time1, Date time2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(time1);
        cal2.setTime(time2);
        return cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
    }
}
