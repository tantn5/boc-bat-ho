package com.os.bbh.common;

import com.os.bbh.mysql.AppAdminConfig;
import com.os.bbh.mysql.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.File;
import java.util.List;

public class Config {
    private static final String TAG = Config.class.getSimpleName();

//    public static final String APP_CONF_PATH = System.getProperty("user.dir") + File.separator + "config" + File.separator + "app.conf";
//    public static final String LOG_CONF_PATH = System.getProperty("user.dir") + File.separator + "config" + File.separator + "log.conf";
//    public static final String FILE_LANGUAGE_PATH = System.getProperty("user.dir") + File.separator + "config" + File.separator + "language.csv";
//    public static final String DBIP_COUNTRY_PATH = System.getProperty("user.dir") + File.separator + "config" + File.separator + "dbip-country-2018-05.csv";

    public static AppAdminConfig adminConfig;

    public static void init() {
        try {
            Utils.log(TAG, "Start Init Config");
            initAdminConfig();
            Utils.log(TAG, "Finish Init Config");
        } catch (Exception e) {
            Utils.error(TAG, e);
        }
    }

    public static void initAdminConfig() {
        Session session = HibernateUtils.getSession();//open session
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            List<AppAdminConfig> list = session.createQuery("from AppAdminConfig").setMaxResults(1).list();
            Config.adminConfig = list.get(0);

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            Utils.error(TAG, e);
        }
    }

//    public static void initServerConfig() {
//        try {
//            Map<String, Object> map = new HashMap<>();
//            Properties properties = new Properties();
//            properties.load(new FileInputStream(APP_CONF_PATH));
//            for (String key : properties.stringPropertyNames()) {
//                String value = properties.getProperty(key);
//                map.put(key, value);
//            }
//            ObjectMapper mapper = new ObjectMapper();
//            SERVER_CONFIG = new ServerConfig();
//            SERVER_CONFIG = mapper.convertValue(map, ServerConfig.class);
//        } catch (Exception e) {
//            Utils.error(TAG, e);
//        }
//    }
}
