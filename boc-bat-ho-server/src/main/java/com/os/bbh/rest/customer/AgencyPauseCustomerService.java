package com.os.bbh.rest.customer;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.GcmUtils;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppCustomer;
import com.os.bbh.mysql.AppNotify;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;

@Path("/")
public class AgencyPauseCustomerService {
    private static final String TAG = AgencyPauseCustomerService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyPauseCustomer")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("customerId") int customerId,
                             @FormParam("active") int active) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppCustomer appCustomer = CacheManager.getCustomer(session, customerId);
                if (appCustomer == null) {
                    status = Status.CUSTOMER_NOT_EXIST;
                } else if (!Utils.checkChildOrIsMe(session, appCustomer.getAgencyId(), agency.appAgency.getId())) {
                    status = Status.PERMISSION_DENIED;
                } else {
                    appCustomer.setState(active == 1 ? Constant.CustomerState.ACTIVE : Constant.CustomerState.PAUSE);
                    session.update(appCustomer);

                    //create notify
                    AppNotify appNotify = new AppNotify();
                    appNotify.setType(Constant.NotifyType.PARENT_PAUSE_CUSTOMER);
                    appNotify.setOwnerId(agency.appAgency.getParentId() > 0 ? agency.appAgency.getParentId() : agency.appAgency.getId());
                    appNotify.setCustomerId(customerId);
                    appNotify.setDes(String.format(active==1?"Đại lý mẹ đã active khách hàng %s":"Đại lý mẹ đã dừng khách hàng %s", appCustomer.getName()));
                    appNotify.setCreated(new Timestamp(System.currentTimeMillis()));
                    session.save(appNotify);
                    GcmUtils.sendNotification(session, appNotify.getOwnerId(), appNotify.getDes(), Utils.toString(appNotify));

                    //check là đại lý mẹ pause mà khách hàng là của đại lý con thì phải tạo notify cho cả đại lý con
                    if (agency.appAgency.getParentId() == 0 && appCustomer.getAgencyId() != agency.appAgency.getId()) {
                        AppNotify appNotify2 = new AppNotify();
                        appNotify2.setType(Constant.NotifyType.PARENT_PAUSE_CUSTOMER);
                        appNotify2.setOwnerId(appCustomer.getAgencyId());
                        appNotify2.setCustomerId(customerId);
                        appNotify2.setDes(String.format(active==1?"Đại lý mẹ đã active khách hàng %s":"Đại lý mẹ đã dừng khách hàng %s", appCustomer.getName()));
                        appNotify2.setCreated(new Timestamp(System.currentTimeMillis()));
                        session.save(appNotify2);
                        GcmUtils.sendNotification(session, appNotify2.getOwnerId(), appNotify.getDes(), Utils.toString(appNotify2));
                    }

                    status = Status.SUCCESSFULL;
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
