package com.os.bbh.rest.agency;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.agency.response.AgencyLoginResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;

@Path("/")
public class AgencyLoginService {
    private static final String TAG = AgencyLoginService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyLogin")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@FormParam("username") String username,
                             @FormParam("password") String password) {
        AgencyLoginResponse response = new AgencyLoginResponse();
        Status status = Status.OPERATION_FAIL;
        if (!Utils.isValidUsername(username)) {
            status = Status.INVALID_USERNAME;
        } else if (!Utils.isValidPassword(password)) {
            status = Status.INVALID_PASSWORD;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppAgency appAgency = CacheManager.getAgency(session, username, password);
                if (appAgency != null) {
                    if (appAgency.expired()) {
                        status = Status.AGENCY_EXPIRED;
                    } else {
                        String sessionId = Utils.randStr(16);
                        Agency agency = new Agency();
                        agency.sessionTicket = sessionId;
                        agency.appAgency = appAgency;
                        AgencyManager.addAgency(agency);

                        response.sessionTicket = sessionId;
                        response.agencyInfo = appAgency.getAgencyInfo();
                        status = Status.SUCCESSFULL;
                    }

                } else {
                    status = Status.INVALID_ACCOUNT_OR_PASSWORD;
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
