package com.os.bbh.rest.topup;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.GcmUtils;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.*;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.transfermoney.response.AgencyCreateTransferMoneyResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;

@Path("/")
public class AgencyCreateTopupService {
    private static final String TAG = AgencyCreateTopupService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyCreateTopup")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("money") double money) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else if (agency.appAgency.getParentId() > 0) {
            status = Status.PERMISSION_DENIED;
        } else if (money <= 0) {
            status = Status.MONEY_NOT_ZERO;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppAgency appAgency = agency.appAgency;
                appAgency.updateMoney(money);
                session.update(appAgency);

                AppStatistic appStatistic = new AppStatistic();
                appStatistic.setAgencyId(agency.appAgency.getId());
                appStatistic.setMoney(money);
                appStatistic.setType(Constant.StatisticType.TOPUP_MONEY);
                session.save(appStatistic);

                status = Status.SUCCESSFULL;

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
                status = new Status(201, e.getMessage());
            }
        }
        response.setStatus(status);
        return response;
    }
}
