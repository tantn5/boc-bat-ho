package com.os.bbh.rest.voucher;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.GcmUtils;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.*;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.voucher.response.AgencyActionVoucherResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;

@Path("/")
public class AgencyActionVoucherService {
    private static final String TAG = AgencyActionVoucherService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyActionVoucher")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("voucherId") int voucherId,
                             @FormParam("money") double money,
                             @FormParam("action") int action) {
        AgencyActionVoucherResponse response = new AgencyActionVoucherResponse();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppAgency appAgency = agency.appAgency;
                AppVoucher appVoucher = CacheManager.getVoucher(session, voucherId);
                if (appVoucher != null) {
                    //check xem agency này là mẹ hay child
                    boolean handlerAble = Utils.checkChildOrIsMe(session, appVoucher.getAgencyId(), appAgency.getId());

                    if (handlerAble) {
                        appVoucher.setAction(action);
                        if (action == Constant.VoucherAction.IN_DEBT) {
                            appVoucher.setState(Constant.VoucherState.PAUSE);
                        }

                        if (action == Constant.VoucherAction.COLLECTED_DEBT) {
                            money = money > 0 ? money : appVoucher.getNextCollectMoney();
                            appVoucher.collectDebt(money);

                            appAgency.updateMoney(money);
                            session.update(appAgency);

                            AppCustomer appCustomer = CacheManager.getCustomer(session, appVoucher.getCustomerId());
                            appCustomer.updateRemainDebt(-money);
                            session.update(appCustomer);

                            //save dashboard
                            AppStatistic appStatistic = new AppStatistic();
                            appStatistic.setAgencyId(agency.appAgency.getId());
                            appStatistic.setMoney(money);
                            appStatistic.setType(Constant.StatisticType.COLLECT_DEBT);
                            appStatistic.setAppVoucherId(appVoucher.getId());
                            session.save(appStatistic);

                            //todo: dashboard lãi

                        }
                        session.update(appVoucher);

                        response.voucherInfo = appVoucher.getVoucherInfo();

                        if (action == Constant.VoucherAction.COLLECTED_DEBT) {
                            if (agency.appAgency.getParentId() > 0) {
                                AppCustomer appCustomer = CacheManager.getCustomer(session, appVoucher.getCustomerId());

                                AppNotify appNotify = new AppNotify();
                                appNotify.setOwnerId(agency.appAgency.getParentId());
                                appNotify.setVoucherId(voucherId);
                                appNotify.setType(Constant.NotifyType.CHILD_COLLECTED_DEBT_CUSTOMER);
                                appNotify.setDes(String.format("Đại lý con %s đã thu %s cuả khách hàng %s", agency.appAgency.getName(), Utils.formatNumber(money), appCustomer.getName()));
                                appNotify.setCreated(new Timestamp(System.currentTimeMillis()));
                                session.save(appNotify);

                                GcmUtils.sendNotification(session, appNotify.getOwnerId(),appNotify.getDes(), Utils.toString(appNotify));
                            }
                        }

                        status = Status.SUCCESSFULL;
                    } else {
                        status = Status.OPERATION_FAIL;
                    }
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }

}
