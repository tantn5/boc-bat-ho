package com.os.bbh.rest.agency.response;

import java.util.Date;

public class AgencyInfo {
    public int id;
    public int parentId;
    public String username;
    public String phone;
    public String name;
    public String address;
    public double money;
    public Date expired;
    public Date created;
}
