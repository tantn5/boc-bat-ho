package com.os.bbh.rest.agency;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppKey;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.agency.response.AgencyAddChildResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;

@Path("/")
public class AgencyAddKeyService {
    private static final String TAG = AgencyAddKeyService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyAddKey")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@FormParam("username") String username,
                             @FormParam("key") String key) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;

        Session session = HibernateUtils.getSession();//open session
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            AppAgency agency = CacheManager.getAgency(session, username);
            if (agency == null) {
                status = Status.AGENCY_NOT_EXIST;
            } else {
                AppKey appKey = CacheManager.getKey(session, key);
                if (appKey == null) {
                    status = Status.KEY_NOT_EXIST;
                } else if (appKey.getUseDate() != null && appKey.getUseDate().getTime() > 0) {
                    status = Status.KEY_USED;
                } else {
                    if (agency.getExpired() == null) {
                        agency.setExpired(new Timestamp(System.currentTimeMillis() + appKey.getTime()));
                    } else if (agency.getExpired().getTime() < System.currentTimeMillis()) {
                        agency.setExpired(new Timestamp(System.currentTimeMillis() + appKey.getTime()));
                    } else {
                        agency.setExpired(new Timestamp(agency.getExpired().getTime() + appKey.getTime()));
                    }
                    session.update(agency);
                    appKey.setUseDate(new Timestamp(System.currentTimeMillis()));
                    session.update(appKey);
                }
            }

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            Utils.error(TAG, e);
        }
        response.setStatus(status);
        return response;
    }
}
