package com.os.bbh.rest.admin.response;

import com.os.bbh.rest.ResponseBase;

public class AdminLoginResponse extends ResponseBase {
    public String sessionTicket;
    public AdminInfo adminInfo;
}
