package com.os.bbh.rest.transfermoney;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppTransferMoney;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.agency.response.AgencyAddChildResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;
import java.util.List;

@Path("/")
public class AgencyDeleteTransferMoneyService {
    private static final String TAG = AgencyDeleteTransferMoneyService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyDeleteTransferMoney")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("transferMoneyId") int transferMoneyId) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else if (agency.appAgency.getParentId() > 0) {
            status = Status.PERMISSION_DENIED;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                List<AppTransferMoney> list = session.createQuery("from AppTransferMoney where id=:transferMoneyId")
                        .setParameter("transferMoneyId", transferMoneyId)
                        .setMaxResults(1).list();
                if (list != null && list.size() > 0) {
                    AppTransferMoney appTransferMoney = list.get(0);
                    if (appTransferMoney.getParentId() != agency.appAgency.getId()) {
                        status = Status.PERMISSION_DENIED;
                    } else {
                        if (appTransferMoney.getState() != Constant.TransferMoneyState.WAITING) {
                            status = Status.OPERATION_FAIL;
                        } else {
                            session.delete(appTransferMoney);
                            status = Status.SUCCESSFULL;
                        }
                    }
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
