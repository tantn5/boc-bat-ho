package com.os.bbh.rest.agency.response;

import com.os.bbh.rest.ResponseBase;

public class AgencyGetInfoResponse extends ResponseBase {
    public int agencyId;
    public AgencyInfo agencyInfo;
}
