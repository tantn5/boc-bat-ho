package com.os.bbh.rest.customer;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;

@Path("/")
public class AgencyAddCustomerService {
    private static final String TAG = AgencyAddCustomerService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyAddCustomer")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("name") String name,
                             @FormParam("phone") String phone,
                             @FormParam("address") String address,
                             @FormParam("cmnd") String cmnd,
                             @FormParam("hokhau") String hokhau,
                             @FormParam("birthDay") long birthDay) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (name == null) name = "Customer " + Utils.randInt(10000, 99999);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
            //} else if (!Utils.isValidName(name)) {
            //    status = Status.INVALID_NAME;
        //} else if (!Utils.isValidPhone(phone)) {
            //status = Status.INVALID_PHONE;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                CacheManager.createCustomer(session, agency.appAgency.getId(), name, phone, address, birthDay, cmnd, hokhau);
                status = Status.SUCCESSFULL;

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
