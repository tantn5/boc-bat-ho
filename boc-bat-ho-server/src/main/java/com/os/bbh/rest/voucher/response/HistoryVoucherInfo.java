package com.os.bbh.rest.voucher.response;

public class HistoryVoucherInfo {
    public long date;
    public double money;
    public int action;

    public class HistoryVoucherAction{
        public static final int COLLECT_DEBT = 0;
    }
}
