package com.os.bbh.rest.agency;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.agency.response.AgencyAddChildResponse;
import com.os.bbh.rest.agency.response.AgencyGetListChildResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.util.List;

@Path("/")
public class AgencyGetListChildService {
    private static final String TAG = AgencyGetListChildService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyGetListChild")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("fromPos") int fromPos,
                             @FormParam("pageSize") int pageSize) {
        AgencyGetListChildResponse response = new AgencyGetListChildResponse();
        Status status = Status.OPERATION_FAIL;
        if (pageSize == 0) pageSize = 10;
        response.fromPos = fromPos;
        response.pageSize = pageSize;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                List<AppAgency> list = session.createQuery("from AppAgency where parentId=:parentId")
                        .setParameter("parentId", agency.appAgency.getId())
                        .setFirstResult(fromPos)
                        .setMaxResults(pageSize)
                        .list();
                if (list != null) {
                    for (AppAgency appAgency : list) {
                        response.agencyInfos.add(appAgency.getAgencyInfo());
                    }
                }
                status = Status.SUCCESSFULL;

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
