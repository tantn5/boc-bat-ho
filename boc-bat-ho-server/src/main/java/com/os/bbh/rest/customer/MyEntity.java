package com.os.bbh.rest.customer;

import org.jboss.resteasy.annotations.providers.multipart.PartType;

import javax.ws.rs.FormParam;

public class MyEntity {
    public MyEntity() {
    }
    @FormParam("customer_id")
    private int customerId;

    @FormParam("image_data")
    @PartType("application/octet-stream")
    private byte[] data;

    public byte[] getData() {
        return data;
    }
    public void setData(byte[] data) {
        this.data = data;
    }

    public int getCustomerId(){return customerId; }

    public void setCustomerId(int customerId){this.customerId = customerId;}
}
