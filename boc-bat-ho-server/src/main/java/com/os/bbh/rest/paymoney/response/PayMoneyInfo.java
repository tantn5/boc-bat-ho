package com.os.bbh.rest.paymoney.response;

public class PayMoneyInfo {
    public int payMoneyId;
    public int agencyId;
    public double money;
    public String purpose;
    public long payDate;
    public int type;
    public int paid;
    public long created;
}
