package com.os.bbh.rest;

public class ResponseBase {
    public int code;
    public String msg;

    public void setStatus(Status status) {
        this.code = status.code;
        this.msg = status.msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
