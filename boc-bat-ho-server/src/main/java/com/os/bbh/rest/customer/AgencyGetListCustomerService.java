package com.os.bbh.rest.customer;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppCustomer;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.customer.response.AgencyGetListCustomerResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.util.ArrayList;
import java.util.List;

@Path("/")
public class AgencyGetListCustomerService {
    private static final String TAG = AgencyGetListCustomerService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyGetListCustomer")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("agencyId") int agencyId,
                             @FormParam("state") int state,
                             @FormParam("fromPos") int fromPos,
                             @FormParam("pageSize") int pageSize) {
        AgencyGetListCustomerResponse response = new AgencyGetListCustomerResponse();
        Status status = Status.OPERATION_FAIL;
        if (pageSize == 0) pageSize = 10;
        response.fromPos = fromPos;
        response.pageSize = pageSize;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                if (agencyId == 0) {
                    agencyId = agency.appAgency.getId();
                }
                List<Integer> listAgencyId = new ArrayList<>();
                listAgencyId.add(agencyId);
                listAgencyId.addAll(Utils.getListChildId(session, agencyId));
                List<AppCustomer> list = new ArrayList<>();
                if (state == Constant.CustomerState.ALL) {
                    list = session.createQuery("from AppCustomer where (agencyId IN (?1) or createId IN (?1))")
                            .setParameterList("1", listAgencyId)
                            .setFirstResult(fromPos)
                            .setMaxResults(pageSize)
                            .list();
                } else {
                    list = session.createQuery("from AppCustomer where (agencyId IN (?1) or createId IN (?1)) and state=:state")
                            .setParameterList("1", listAgencyId)
                            .setParameter("state", state)
                            .setFirstResult(fromPos)
                            .setMaxResults(pageSize)
                            .list();
                }

                if (list != null) {
                    for (AppCustomer appCustomer : list) {
                        response.customerInfos.add(appCustomer.getCustomerInfo(session));
                    }
                }
                status = Status.SUCCESSFULL;

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
