package com.os.bbh.rest.dashboard.response;

import com.os.bbh.rest.ResponseBase;

import java.util.ArrayList;
import java.util.List;

public class AgencyGetDashBoard2Response extends ResponseBase {
    public long fromDate;
    public long toDate;
    public List<DashBoardItem1> list = new ArrayList<>();
}
