package com.os.bbh.rest.transfermoney;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppStatistic;
import com.os.bbh.mysql.AppTransferMoney;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.util.List;

@Path("/")
public class AgencyActionTransferMoneyService {
    private static final String TAG = AgencyActionTransferMoneyService.class.getSimpleName();

    private static final int ACTION_ACCEPT = 0;
    private static final int ACTION_REJECT = 1;

    @POST // This annotation indicates GET request
    @Path("/AgencyActionTransferMoney")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("transferMoneyId") int transferMoneyId,
                             @FormParam("action") int action) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else if (agency.appAgency.getParentId() == 0) {
            status = Status.PERMISSION_DENIED;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppAgency childAppAgency = agency.appAgency;
                AppAgency parentAppAgency = CacheManager.getAgencyById(session, childAppAgency.getParentId());
                if (parentAppAgency != null) {
                    List<AppTransferMoney> list = session.createQuery("from AppTransferMoney where id=:transferMoneyId")
                            .setParameter("transferMoneyId", transferMoneyId)
                            .setMaxResults(1).list();
                    if (list != null && list.size() > 0) {
                        AppTransferMoney appTransferMoney = list.get(0);
                        if (appTransferMoney.getChildId() != childAppAgency.getId() || appTransferMoney.getParentId() != parentAppAgency.getId()) {
                            status = Status.PERMISSION_DENIED;
                        } else if (appTransferMoney.getState() != Constant.TransferMoneyState.WAITING) {
                            status = Status.OPERATION_FAIL;
                        } else {
                            if (action == ACTION_ACCEPT) {
                                if (parentAppAgency.enough(appTransferMoney.getMoney())) {
                                    appTransferMoney.setState(Constant.TransferMoneyState.ACCEPT);
                                    session.update(appTransferMoney);
                                    status = Status.SUCCESSFULL;

                                    //cong quy con
                                    childAppAgency.updateMoney(appTransferMoney.getMoney());
                                    session.update(childAppAgency);

                                    //tru quy me
                                    parentAppAgency.updateMoney(-appTransferMoney.getMoney());
                                    session.update(parentAppAgency);

                                    //save thong ke
                                    AppStatistic appStatistic = new AppStatistic();
                                    appStatistic.setAgencyId(childAppAgency.getId());
                                    appStatistic.setMoney(appTransferMoney.getMoney());
                                    appStatistic.setType(Constant.StatisticType.CHILD_GET_TRANSFER_FROM_PARENT);
                                    appStatistic.setAppTransferMoneyId(appTransferMoney.getId());
                                    session.save(appStatistic);

                                    AppStatistic appStatistic2 = new AppStatistic();
                                    appStatistic2.setAgencyId(parentAppAgency.getId());
                                    appStatistic2.setMoney(appTransferMoney.getMoney());
                                    appStatistic2.setType(Constant.StatisticType.PARENT_TRANSFER_TO_CHILD);
                                    appStatistic2.setAppTransferMoneyId(appTransferMoney.getId());
                                    session.save(appStatistic2);

                                    status = Status.SUCCESSFULL;
                                } else {
                                    status = Status.PARENT_NOT_ENOUGH_MONEY;
                                }

                            } else if (action == ACTION_REJECT) {
                                appTransferMoney.setState(Constant.TransferMoneyState.REJECT);
                                session.update(appTransferMoney);
                                status = Status.SUCCESSFULL;

                                //todo: xử lý khoản chi
                            }
                        }
                    }
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
