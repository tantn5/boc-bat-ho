package com.os.bbh.rest.dashboard;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppStatistic;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.dashboard.response.AgencyGetDashBoardResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;
import java.util.List;

@Path("/")
public class AgencyGetDashBoardService {
    private static final String TAG = AgencyGetDashBoardService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyGetDashBoard")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("fromDate") long fromDate,
                             @FormParam("toDate") long toDate) {
        AgencyGetDashBoardResponse response = new AgencyGetDashBoardResponse();
        response.fromDate = fromDate;
        response.toDate = toDate;
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                List<AppStatistic> list = session.createQuery("from AppStatistic where agencyId=:agencyId and created>=:fromDate and created<=:toDate")
                        .setParameter("agencyId", agency.appAgency.getId())
                        .setParameter("fromDate", new Timestamp(fromDate))
                        .setParameter("toDate", new Timestamp(toDate))
                        .list();
                if (list != null && list.size() > 0) {
                    for (AppStatistic appStatistic : list) {
                        if (appStatistic.getType() == Constant.StatisticType.TOPUP_MONEY) {
                            response.von += appStatistic.getMoney();
                        } else if (appStatistic.getType() == Constant.StatisticType.PARENT_TRANSFER_TO_CHILD) {
                            response.von += appStatistic.getMoney();
                        } else if (appStatistic.getType() == Constant.StatisticType.CHILD_GET_TRANSFER_FROM_PARENT) {
                            response.chi += appStatistic.getMoney();
                        } else if (appStatistic.getType() == Constant.StatisticType.AGENCY_PAID_PAY_MONEY) {
                            response.chi += appStatistic.getMoney();
                        } else if (appStatistic.getType() == Constant.StatisticType.COLLECT_DEBT) {
                            response.thu += appStatistic.getMoney();
                        } else if (appStatistic.getType() == Constant.StatisticType.CREATE_VOUCHER) {
                            response.chi += appStatistic.getMoney();
                        }
                    }
                }
                status = Status.SUCCESSFULL;

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
