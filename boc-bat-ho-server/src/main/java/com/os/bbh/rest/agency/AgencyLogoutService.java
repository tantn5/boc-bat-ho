package com.os.bbh.rest.agency;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;

import javax.ws.rs.*;

@Path("/")
public class AgencyLogoutService {
    private static final String TAG = AgencyLogoutService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyLogout")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            AgencyManager.removeAgency(agency);
            status = Status.SUCCESSFULL;
        }
        response.setStatus(status);
        return response;
    }
}
