package com.os.bbh.rest.notify.response;

import com.os.bbh.rest.ResponseBase;

public class AgencyGetUnreadNotifyResponse extends ResponseBase {
    public int unread;
}
