package com.os.bbh.rest.customer;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.ManyThings;
import com.os.bbh.common.Thing;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppCustomer;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.util.List;

@Path("/")
public class AgencyShareCustomerService {
    private static final String TAG = AgencyShareCustomerService.class.getSimpleName();

    private static final int ACTION_SHARE = 0;
    private static final int ACTION_UN_SHARE = 1;

    @POST // This annotation indicates GET request
    @Path("/AgencyShareCustomer")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("customerIds") ManyThings customerIds,
                             @FormParam("childId") int childId,
                             @FormParam("action") int action) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else if (agency.appAgency.getParentId() > 0) {
            status = Status.PERMISSION_DENIED;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppAgency childAppAgency = CacheManager.getAgencyById(session, childId);
                if (childAppAgency == null || childAppAgency.getParentId() != agency.appAgency.getId()) {
                    status = Status.OPERATION_FAIL;
                } else {
                    for (Thing thing : customerIds.things) {
                        int customerId = Integer.valueOf(thing.value);
                        AppCustomer appCustomer = CacheManager.getCustomer(session, customerId);
                        if (appCustomer.getCreateId() != agency.appAgency.getId()) {
                            continue;
                        } else {
                            if (action == ACTION_SHARE) {
                                appCustomer.setAgencyId(childAppAgency.getId());
                            } else {
                                appCustomer.setAgencyId(agency.appAgency.getId());
                            }
                            session.update(appCustomer);
                            status = Status.SUCCESSFULL;
                        }
                    }
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
