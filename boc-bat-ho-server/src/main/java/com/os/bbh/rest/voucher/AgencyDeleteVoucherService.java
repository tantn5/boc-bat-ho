package com.os.bbh.rest.voucher;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppVoucher;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.voucher.response.AgencyEditVoucherResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;

@Path("/")
public class AgencyDeleteVoucherService {
    private static final String TAG = AgencyDeleteVoucherService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyDeleteVoucher")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("voucherId") int voucherId) {
        AgencyEditVoucherResponse response = new AgencyEditVoucherResponse();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppVoucher appVoucher = CacheManager.getVoucher(session, voucherId);
                if (appVoucher != null) {
                    //check tai khoan mẹ mới có quyền edit
                    if (!Utils.checkParent(session, appVoucher.getAgencyId(), agency.appAgency.getId())) {
                        status = Status.PERMISSION_DENIED;
                    } else {
                        appVoucher.setDeleted(1);
                        session.update(appVoucher);
                        status = Status.SUCCESSFULL;
                    }
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
