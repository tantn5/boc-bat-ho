package com.os.bbh.rest.customer.response;

import com.os.bbh.rest.ResponseBase;

import java.util.ArrayList;
import java.util.List;

public class AgencySearchCustomerResponse extends ResponseBase {
    public String text;
    public int fromPos;
    public int pageSize;
    public List<CustomerInfo> customerInfos = new ArrayList<>();
}
