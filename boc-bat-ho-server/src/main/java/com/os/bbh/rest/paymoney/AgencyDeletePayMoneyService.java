package com.os.bbh.rest.paymoney;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppPayMoney;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.paymoney.response.AgencyCreatePayMoneyResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;
import java.util.List;

@Path("/")
public class AgencyDeletePayMoneyService {
    private static final String TAG = AgencyDeletePayMoneyService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyDeletePayMoney")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("payMoneyId") int payMoneyId) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                List<AppPayMoney> list = session.createQuery("from AppPayMoney where id=:payMoneyId and agencyId=:agencyId")
                        .setParameter("payMoneyId", payMoneyId)
                        .setParameter("agencyId", agency.appAgency.getId())
                        .setMaxResults(1).list();
                if (list != null && list.size() > 0) {
                    AppPayMoney appPayMoney = list.get(0);
                    if (appPayMoney.getPaid() == 0) {
                        appPayMoney.setDeleted(1);
                        session.update(appPayMoney);
                        status = Status.SUCCESSFULL;
                    } else {
                        status = Status.CANNOT_DELETE_BECAUSE_PAID;
                    }
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
