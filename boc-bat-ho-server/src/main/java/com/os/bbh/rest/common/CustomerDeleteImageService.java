package com.os.bbh.rest.common;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppImage;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.customer.MyEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

@Path("/")
public class CustomerDeleteImageService {
    private static final String TAG = CustomerDeleteImageService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/CustomerDeleteImage")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("imageId") int imageId) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;

        Session session = HibernateUtils.getSession();//open session
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            List<AppImage> list = session.createQuery("from AppImage where id=:imageId")
                    .setParameter("imageId", imageId)
                    .setMaxResults(1)
                    .list();
            if (list != null && list.size() > 0) {
                AppImage appImage = list.get(0);
                appImage.setDeleted(1);
                session.save(appImage);
            }

            status = Status.SUCCESSFULL;

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            Utils.error(TAG, e);
        }
        response.setStatus(status);
        return response;
    }
}
