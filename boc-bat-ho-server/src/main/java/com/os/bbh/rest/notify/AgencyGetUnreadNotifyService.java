package com.os.bbh.rest.notify;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppNotify;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.notify.response.AgencyGetUnreadNotifyResponse;
import com.os.bbh.rest.notify.response.AgencyReadNotifyResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.util.List;

@Path("/")
public class AgencyGetUnreadNotifyService {
    private static final String TAG = AgencyGetUnreadNotifyService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyGetUnreadNotify")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket) {
        AgencyGetUnreadNotifyResponse response = new AgencyGetUnreadNotifyResponse();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                List<AppNotify> list = session.createQuery("from AppNotify where ownerId=:ownerId and unread=1")
                        .setParameter("ownerId", agency.appAgency.getId()).list();
                if (list != null && list.size() > 0) {
                    response.unread = list.size();
                }
                status = Status.SUCCESSFULL;

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
