package com.os.bbh.rest.paymoney;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppPayMoney;
import com.os.bbh.mysql.AppVoucher;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.paymoney.response.AgencyCreatePayMoneyResponse;
import com.os.bbh.rest.voucher.response.AgencyCreateVoucherResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;

@Path("/")
public class AgencyCreatePayMoneyService {
    private static final String TAG = AgencyCreatePayMoneyService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyCreatePayMoney")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("money") double money,
                             @FormParam("purpose") String purpose,
                             @FormParam("payDate") long payDate,
                             @FormParam("type") int type) {
        AgencyCreatePayMoneyResponse response = new AgencyCreatePayMoneyResponse();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppPayMoney appPayMoney = new AppPayMoney();
                appPayMoney.setAgencyId(agency.appAgency.getId());
                appPayMoney.setMoney(money);
                appPayMoney.setPurpose(purpose);
                appPayMoney.setType(type);
                appPayMoney.setPayDate(new Timestamp(payDate));
                appPayMoney.setCreated(new Timestamp(System.currentTimeMillis()));
                session.save(appPayMoney);

                response.payMoneyInfo = appPayMoney.getPayMoneyInfo();

                status = Status.SUCCESSFULL;

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
