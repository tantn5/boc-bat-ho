package com.os.bbh.rest.notify.response;

public class NotifyInfo {
    public int notifyId;
    public int ownerId;
    public int parentId;
    public int childId;
    public int customerId;
    public int voucherId;
    public int transferMoneyId;
    public String des;
    public int type;
    public int read;
    public long created;
}
