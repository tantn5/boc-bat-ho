package com.os.bbh.rest.dashboard.response;

import java.util.Date;

public class DashBoardItem2 {
    public int agencyId;
    public double money;
    public int type;
    public String description;
    public int voucherId;
    public int payMoneyId;
    public Date created;
}
