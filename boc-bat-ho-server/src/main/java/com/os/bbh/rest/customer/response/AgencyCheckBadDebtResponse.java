package com.os.bbh.rest.customer.response;

import com.os.bbh.rest.ResponseBase;

import java.util.ArrayList;
import java.util.List;

public class AgencyCheckBadDebtResponse extends ResponseBase {
    public boolean isBadDebt;
    public boolean isOldCustomer;
    public double remainDebt;
    public List<CustomerInfo> customerInfos = new ArrayList<>();
}
