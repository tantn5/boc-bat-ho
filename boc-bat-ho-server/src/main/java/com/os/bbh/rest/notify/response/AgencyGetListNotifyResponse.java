package com.os.bbh.rest.notify.response;

import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.paymoney.response.PayMoneyInfo;

import java.util.ArrayList;
import java.util.List;

public class AgencyGetListNotifyResponse extends ResponseBase {
    public int fromPos;
    public int pageSize;
    public List<NotifyInfo> notifyInfos = new ArrayList<>();
}
