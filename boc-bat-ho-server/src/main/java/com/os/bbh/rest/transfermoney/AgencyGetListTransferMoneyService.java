package com.os.bbh.rest.transfermoney;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppTransferMoney;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.agency.response.AgencyAddChildResponse;
import com.os.bbh.rest.transfermoney.response.AgencyGetListTransferMoneyResponse;
import com.os.bbh.rest.transfermoney.response.TransferMoneyInfo;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;
import java.util.List;

@Path("/")
public class AgencyGetListTransferMoneyService {
    private static final String TAG = AgencyGetListTransferMoneyService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyGetListTransferMoney")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("fromPos") int fromPos,
                             @FormParam("pageSize") int pageSize,
                             @FormParam("childId") int childId,
                             @FormParam("state") int state) {
        AgencyGetListTransferMoneyResponse response = new AgencyGetListTransferMoneyResponse();
        if (pageSize == 0) pageSize = 10;
        response.childId = childId;
        response.fromPos = fromPos;
        response.pageSize = pageSize;
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else if (agency.appAgency.getParentId() > 0) {
            status = Status.PERMISSION_DENIED;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                List<AppTransferMoney> list = null;
                if (state == Constant.TransferMoneyState.ALL) {
                    if (agency.appAgency.getParentId() > 0) {
                        list = session.createQuery("from AppTransferMoney where parentId=:parentId and childId=:childId order by created desc")
                                .setParameter("parentId", agency.appAgency.getParentId())
                                .setParameter("childId", agency.appAgency.getId())
                                .setFirstResult(fromPos).setMaxResults(pageSize).list();
                    } else {
                        if (childId == 0) {
                            list = session.createQuery("from AppTransferMoney where parentId=:parentId order by created desc")
                                    .setParameter("parentId", agency.appAgency.getId())
                                    .setFirstResult(fromPos).setMaxResults(pageSize).list();
                        } else {
                            list = session.createQuery("from AppTransferMoney where parentId=:parentId and childId=:childId order by created desc")
                                    .setParameter("parentId", agency.appAgency.getId())
                                    .setParameter("childId", childId)
                                    .setFirstResult(fromPos).setMaxResults(pageSize).list();
                        }
                    }
                } else {
                    if (agency.appAgency.getParentId() > 0) {
                        list = session.createQuery("from AppTransferMoney where parentId=:parentId and childId=:childId and state=:state order by created desc")
                                .setParameter("parentId", agency.appAgency.getParentId())
                                .setParameter("childId", agency.appAgency.getId())
                                .setParameter("state", state)
                                .setFirstResult(fromPos).setMaxResults(pageSize).list();
                    } else {
                        if (childId == 0) {
                            list = session.createQuery("from AppTransferMoney where parentId=:parentId and state=:state order by created desc")
                                    .setParameter("parentId", agency.appAgency.getId())
                                    .setParameter("state", state)
                                    .setFirstResult(fromPos).setMaxResults(pageSize).list();
                        } else {
                            list = session.createQuery("from AppTransferMoney where parentId=:parentId and childId=:childId and state=:state order by created desc")
                                    .setParameter("parentId", agency.appAgency.getId())
                                    .setParameter("state", state)
                                    .setParameter("childId", childId)
                                    .setFirstResult(fromPos).setMaxResults(pageSize).list();
                        }
                    }
                }
                if (list != null && list.size() > 0) {
                    for (AppTransferMoney appTransferMoney : list) {
                        response.transferMoneyInfos.add(appTransferMoney.getTransferMoneyInfo(session));
                    }
                }
                status = Status.SUCCESSFULL;

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
