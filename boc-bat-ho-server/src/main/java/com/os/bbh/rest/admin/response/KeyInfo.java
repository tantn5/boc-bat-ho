package com.os.bbh.rest.admin.response;

import java.sql.Date;

public class KeyInfo {
    public String key;
    public long time;
    public int active;
    public int agencyId;//agency active key nay
    public String createBy;
    public long useDate;
    public long created;
}
