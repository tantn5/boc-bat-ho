package com.os.bbh.rest.agency;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.agency.response.AgencyAddChildResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;

@Path("/")
public class AgencyAddChildService {
    private static final String TAG = AgencyAddChildService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyAddChild")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("username") String username,
                             @FormParam("phone") String phone,
                             @FormParam("name") String name,
                             @FormParam("address") String address,
                             @FormParam("password") String password) {
        AgencyAddChildResponse response = new AgencyAddChildResponse();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (name == null) name = "AGC " + Utils.randInt(10000, 99999);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else if (agency.appAgency.getParentId() > 0) {
            status = Status.PERMISSION_DENIED;
//        } else if (!Utils.isValidPhone(phone)) {
//            //status = Status.INVALID_PHONE;
            //} else if (!Utils.isValidName(name)) {
            //status = Status.INVALID_NAME;
        } else if (!Utils.isValidUsername(username)) {
            status = Status.INVALID_USERNAME;
        } else if (!Utils.isValidPassword(password)) {
            status = Status.INVALID_PASSWORD;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppAgency appAgency = CacheManager.getAgency(session, username);
                if (appAgency != null) {
                    status = Status.ACCOUNT_EXIST;
                } else {
                    appAgency = CacheManager.createAgency(session, username, password, phone, name, address, agency.appAgency.getId());
                    if (appAgency == null) {
                        status = Status.OPERATION_FAIL;
                    } else {
                        response.agencyInfo = appAgency.getAgencyInfo();
                        status = Status.SUCCESSFULL;
                    }
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
