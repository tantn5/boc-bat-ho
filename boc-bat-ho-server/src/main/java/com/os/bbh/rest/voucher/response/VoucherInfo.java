package com.os.bbh.rest.voucher.response;

public class VoucherInfo {
    public int voucherId;
    public int type;
    public int agencyId;
    public int customerId;
    public long startDate;
    public long endDate;
    public int numDay;
    public int periodThuLai;//chu kì thu lãi - theo ngày
    public double loan;//tiền vay ban đầu
    public double remainLoan;//số tiền còn lại của khoản vay
    public double lai;//số tiền còn lại của khoản vay
    public double interestRateLai;//lãi suất vay lãi
    public float interestRateHo;//lãi suất bốc họ
    public String reference;
    public String referencePhone;
    public String referenceDes;
    public int state;//trạng thái của chứng từ
    public int action;
    public long created;
    public long nextCollectDay;//ngày thu tiền sắp tới
    public double nextCollectMoney;//số tiền thu sắp tới
}
