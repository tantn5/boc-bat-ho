package com.os.bbh.rest.transfermoney.response;

import com.os.bbh.rest.ResponseBase;

import java.util.ArrayList;
import java.util.List;

public class AgencyGetListTransferMoneyResponse extends ResponseBase {
    public int childId;
    public int fromPos;
    public int pageSize;
    public List<TransferMoneyInfo> transferMoneyInfos = new ArrayList<>();
}
