package com.os.bbh.rest.admin;

import com.os.bbh.app.AdminManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Admin;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppKey;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.admin.response.AdminCreateKeyResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;

@Path("/")
public class AdminLogoutService {
    private static final String TAG = AdminLogoutService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AdminLogout")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;
        Admin admin = AdminManager.getAdminBySessionTicket(sessionTicket);
        if (admin == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            AdminManager.removeAdmin(sessionTicket);
            status = Status.SUCCESSFULL;
        }
        response.setStatus(status);
        return response;
    }
}
