package com.os.bbh.rest.notify;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppNotify;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.notify.response.AgencyGetListNotifyResponse;
import com.os.bbh.rest.notify.response.AgencyReadNotifyResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.util.List;

@Path("/")
public class AgencyReadNotifyService {
    private static final String TAG = AgencyReadNotifyService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyReadNotify")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("notifyId") int notifyId) {
        AgencyReadNotifyResponse response = new AgencyReadNotifyResponse();
        response.notifyId = notifyId;
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                List<AppNotify> list = session.createQuery("from AppNotify where id=:notifyId")
                        .setParameter("notifyId", notifyId).setMaxResults(1).list();
                if (list != null && list.size() > 0) {
                    AppNotify appNotify = list.get(0);
                    if (appNotify.getOwnerId() == agency.appAgency.getId()) {
                        appNotify.setUnread(0);
                        session.update(appNotify);
                        status = Status.SUCCESSFULL;
                    }
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
