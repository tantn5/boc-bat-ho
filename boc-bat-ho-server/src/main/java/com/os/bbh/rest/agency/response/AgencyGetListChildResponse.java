package com.os.bbh.rest.agency.response;

import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.customer.response.CustomerInfo;

import java.util.ArrayList;
import java.util.List;

public class AgencyGetListChildResponse extends ResponseBase {
    public int fromPos;
    public int pageSize;
    public List<AgencyInfo> agencyInfos = new ArrayList<>();
}
