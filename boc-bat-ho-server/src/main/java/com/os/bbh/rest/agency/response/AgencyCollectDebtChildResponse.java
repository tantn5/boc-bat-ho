package com.os.bbh.rest.agency.response;

import com.os.bbh.rest.ResponseBase;

public class AgencyCollectDebtChildResponse extends ResponseBase {
    public AgencyInfo childAgencyInfo;
    public AgencyInfo parentAgencyInfo;
}
