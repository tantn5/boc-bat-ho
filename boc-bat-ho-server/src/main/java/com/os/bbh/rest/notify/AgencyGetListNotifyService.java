package com.os.bbh.rest.notify;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppCustomer;
import com.os.bbh.mysql.AppNotify;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.customer.response.AgencyGetListCustomerResponse;
import com.os.bbh.rest.notify.response.AgencyGetListNotifyResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.util.ArrayList;
import java.util.List;

@Path("/")
public class AgencyGetListNotifyService {
    private static final String TAG = AgencyGetListNotifyService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyGetListNotify")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("fromPos") int fromPos,
                             @FormParam("pageSize") int pageSize) {
        AgencyGetListNotifyResponse response = new AgencyGetListNotifyResponse();
        Status status = Status.OPERATION_FAIL;
        if (pageSize == 0) pageSize = 10;
        response.fromPos = fromPos;
        response.pageSize = pageSize;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                //get list child
                List<AppNotify> listChild = session.createQuery("from AppNotify where ownerId=:ownerId order by created desc")
                        .setParameter("ownerId", agency.appAgency.getId()).list();
                if (listChild != null && listChild.size() > 0) {
                    for (AppNotify appNotify : listChild) {
                        response.notifyInfos.add(appNotify.getNotifyInfo(session));
                    }
                }
                status = Status.SUCCESSFULL;

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
