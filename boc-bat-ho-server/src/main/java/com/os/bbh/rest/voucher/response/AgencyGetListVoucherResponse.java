package com.os.bbh.rest.voucher.response;

import com.os.bbh.rest.ResponseBase;

import java.util.ArrayList;
import java.util.List;

public class AgencyGetListVoucherResponse extends ResponseBase {
    public int fromPos;
    public int pageSize;
    public List<VoucherInfo> voucherInfos = new ArrayList<>();
}
