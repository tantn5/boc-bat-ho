package com.os.bbh.rest.agency;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.GcmUtils;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppNotify;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.agency.response.AgencyCollectDebtChildResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;

@Path("/")
public class AgencyCollectDebtChildService {
    private static final String TAG = AgencyCollectDebtChildService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyCollectDebtChild")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("childId") int childId,
                             @FormParam("money") double money) {
        AgencyCollectDebtChildResponse response = new AgencyCollectDebtChildResponse();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppAgency parentAppAgency = agency.appAgency;
                AppAgency childAppAgency = CacheManager.getAgencyById(session, childId);
                if (childAppAgency == null || childAppAgency.getParentId() != agency.appAgency.getId()) {
                    status = Status.OPERATION_FAIL;
                } else {
                    if (!childAppAgency.enough(money)) {
                        status = Status.NOT_ENOUGH_MONEY;
                    } else {
                        childAppAgency.updateMoney(-money);
                        session.update(childAppAgency);

                        parentAppAgency.updateMoney(money);
                        session.update(parentAppAgency);

                        response.childAgencyInfo = childAppAgency.getAgencyInfo();
                        response.parentAgencyInfo = parentAppAgency.getAgencyInfo();
                        status = Status.SUCCESSFULL;

                        //todo: luu khoan thu chi

                        //create notify
                        AppNotify appNotify = new AppNotify();
                        appNotify.setType(Constant.NotifyType.PARENT_COLLECT_DEBT_CHILD);
                        appNotify.setParentId(agency.appAgency.getId());
                        appNotify.setChildId(childId);
                        appNotify.setOwnerId(childId);
                        appNotify.setDes("Đại lý mẹ đã thu " + Utils.formatNumber(money));
                        appNotify.setCreated(new Timestamp(System.currentTimeMillis()));
                        session.save(appNotify);

                        GcmUtils.sendNotification(session, appNotify.getOwnerId(), appNotify.getDes(), Utils.toString(appNotify));
                    }
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
