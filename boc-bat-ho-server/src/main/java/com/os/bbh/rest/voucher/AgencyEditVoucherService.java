package com.os.bbh.rest.voucher;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppVoucher;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.voucher.response.AgencyEditVoucherResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;
import java.util.List;

@Path("/")
public class AgencyEditVoucherService {
    private static final String TAG = AgencyEditVoucherService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyEditVoucher")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("voucherId") int voucherId,
                             @FormParam("startDate") long startDate,
                             @FormParam("endDate") long endDate,
                             @FormParam("numDay") int numDay,
                             @FormParam("periodThuLai") int periodThuLai,
                             @FormParam("loan") double loan,
                             @FormParam("interestRateLai") double interestRateLai,
                             @FormParam("interestRateHo") float interestRateHo,
                             @FormParam("reference") String reference,
                             @FormParam("referencePhone") String referencePhone,
                             @FormParam("referenceDes") String referenceDes) {
        AgencyEditVoucherResponse response = new AgencyEditVoucherResponse();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppVoucher appVoucher = CacheManager.getVoucher(session, voucherId);
                if (appVoucher != null) {
                    //check tai khoan mẹ mới có quyền edit
                    AppAgency appAgency2 = CacheManager.getAgencyById(session, appVoucher.getAgencyId());
                    if (appAgency2.getParentId() == agency.appAgency.getId()) {
                        if (startDate > 0) appVoucher.setStartDate(new Timestamp(startDate));
                        if (endDate > 0) appVoucher.setEndDate(new Timestamp(endDate));
                        appVoucher.setNumDay(numDay);
                        appVoucher.setPeriodThuLai(periodThuLai);
                        appVoucher.setLoan(loan);
                        appVoucher.setInterestRateLai(interestRateLai);
                        appVoucher.setInterestRateHo(interestRateHo);
                        appVoucher.setReference(reference);
                        appVoucher.setReferencePhone(referencePhone);
                        appVoucher.setReferenceDes(referenceDes);
                        session.update(appVoucher);

                        response.voucherInfo = appVoucher.getVoucherInfo();
                        status = Status.SUCCESSFULL;
                    } else {
                        status = Status.CHILD_AGENCY_CANNOT_EDIT_VOUCHER;
                    }
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
