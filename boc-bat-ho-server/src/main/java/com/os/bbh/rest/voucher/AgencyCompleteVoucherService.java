package com.os.bbh.rest.voucher;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.GcmUtils;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.*;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.voucher.response.AgencyChangeStateVoucherResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;
import java.util.List;

@Path("/")
public class AgencyCompleteVoucherService {
    private static final String TAG = AgencyCompleteVoucherService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyCompleteVoucher")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("voucherId") int voucherId) {
        AgencyChangeStateVoucherResponse response = new AgencyChangeStateVoucherResponse();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppVoucher appVoucher = CacheManager.getVoucher(session, voucherId);
                if (appVoucher != null) {
                    if (Utils.checkChildOrIsMe(session, appVoucher.getAgencyId(), agency.appAgency.getId())) {
                        double collectMoney = appVoucher.getRemainLoan();

                        AppAgency appAgency = agency.appAgency;
                        appAgency.updateMoney(collectMoney);
                        session.update(appAgency);

                        appVoucher.setState(Constant.VoucherState.COMPLETE);
                        appVoucher.setRemainLoan(0);
                        appVoucher.collectDebt(collectMoney);
                        session.update(appVoucher);
                        response.voucherInfo = appVoucher.getVoucherInfo();

                        AppCustomer appCustomer = CacheManager.getCustomer(session, appVoucher.getCustomerId());
                        List<AppVoucher> listVoucher = session.createQuery("from AppVoucher where customerId=:customerId and deleted=0 and state<>:state")
                                .setParameter("customerId", appVoucher.getCustomerId()).setParameter("state", Constant.VoucherState.COMPLETE).list();
                        if (listVoucher.size() == 0) {
                            appCustomer.setState(Constant.CustomerState.COMPLETE);
                        }
                        appCustomer.updateRemainDebt(-collectMoney);
                        session.update(appCustomer);

                        AppNotify appNotify = new AppNotify();
                        appNotify.setOwnerId(agency.appAgency.getParentId() > 0 ? agency.appAgency.getParentId() : agency.appAgency.getId());
                        appNotify.setCustomerId(appVoucher.getCustomerId());
                        appNotify.setVoucherId(voucherId);
                        if (agency.appAgency.getParentId() > 0) {
                            appNotify.setType(Constant.NotifyType.CHILD_COMPLETE_VOUCHER);
                            appNotify.setDes(String.format("Đại lý con %s đã tất toán khoản vay %s của khách hàng khách hàng %s", agency.appAgency.getName(), Utils.formatNumber(appVoucher.getLoan()), appCustomer.getName()));
                        } else {
                            appNotify.setType(Constant.NotifyType.PARENT_COMPLETE_VOUCHER);
                            appNotify.setDes(String.format("Đại lý mẹ đã tất toán khoản vay %s của khách hàng khách hàng %s", Utils.formatNumber(appVoucher.getLoan()), appCustomer.getName()));
                        }
                        appNotify.setCreated(new Timestamp(System.currentTimeMillis()));
                        session.save(appNotify);

                        //save dashboard
                        AppStatistic appStatistic = new AppStatistic();
                        appStatistic.setAgencyId(agency.appAgency.getId());
                        appStatistic.setMoney(collectMoney);
                        appStatistic.setType(Constant.StatisticType.COLLECT_DEBT);
                        appStatistic.setAppVoucherId(appVoucher.getId());
                        session.save(appStatistic);

                        //todo: dashboard lãi

                        GcmUtils.sendNotification(session, appNotify.getOwnerId(), appNotify.getDes(), Utils.toString(appNotify));

                        status = Status.SUCCESSFULL;
                    } else {
                        status = Status.OPERATION_FAIL;
                    }
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
