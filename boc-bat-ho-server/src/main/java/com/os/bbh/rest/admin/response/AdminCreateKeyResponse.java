package com.os.bbh.rest.admin.response;

import com.os.bbh.rest.ResponseBase;

public class AdminCreateKeyResponse extends ResponseBase {
    public KeyInfo keyInfo;
}
