package com.os.bbh.rest.paymoney.response;

import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.voucher.response.VoucherInfo;

import java.util.ArrayList;
import java.util.List;

public class AgencyGetListPayMoneyResponse extends ResponseBase {
    public int fromPos;
    public int pageSize;
    public long fromDate;
    public long toDate;
    public List<PayMoneyInfo> payMoneyInfos = new ArrayList<>();
}
