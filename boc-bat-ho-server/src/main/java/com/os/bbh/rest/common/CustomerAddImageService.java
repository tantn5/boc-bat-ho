package com.os.bbh.rest.common;

import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppImage;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.common.response.CustomerAddImageResponse;
import com.os.bbh.rest.customer.MyEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;

@Path("/")
public class CustomerAddImageService {
    private static final String TAG = CustomerAddImageService.class.getSimpleName();

//    public static final String UPLOADED_FILE_PATH = "d:\\";
    public static final String UPLOADED_FILE_PATH = "/home/uploadfiledata/";

    @POST // This annotation indicates GET request
    @Path("/CustomerAddImage")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseBase uploadImage(@MultipartForm MyEntity myEntity) throws Exception {
        CustomerAddImageResponse response = new CustomerAddImageResponse();
        int customerId = myEntity.getCustomerId();
        String fileName = customerId + "_" + Utils.randStr(10) + ".jpg";
        //String fileName = "test.jpg";
        String filePath = UPLOADED_FILE_PATH + fileName;

        writeFile(myEntity.getData(), filePath);

        Session session = HibernateUtils.getSession();//open session
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            AppImage appImage = new AppImage();
            appImage.setCustomerId(customerId);
            appImage.setName(fileName);
            appImage.setCreated(new Timestamp(System.currentTimeMillis()));
            session.save(appImage);

            response.imageInfo = appImage.getImageInfo();

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            Utils.error(TAG, e);
        }

        response.setStatus(Status.SUCCESSFULL);
        return response;
    }

    //save to somewhere
    private void writeFile(byte[] content, String filename) throws IOException {

        File file = new File(filename);

        if (!file.exists()) {
            file.createNewFile();
        }

        FileOutputStream fop = new FileOutputStream(file);

        fop.write(content);
        fop.flush();
        fop.close();

    }
}
