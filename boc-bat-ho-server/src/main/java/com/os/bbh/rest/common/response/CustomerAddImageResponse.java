package com.os.bbh.rest.common.response;

import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.customer.response.CustomerInfo;
import com.os.bbh.rest.customer.response.ImageInfo;

import java.util.ArrayList;
import java.util.List;

public class CustomerAddImageResponse extends ResponseBase {
    public ImageInfo imageInfo;
}
