package com.os.bbh.rest.transfermoney;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.GcmUtils;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppNotify;
import com.os.bbh.mysql.AppTransferMoney;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.transfermoney.response.AgencyCreateTransferMoneyResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;

@Path("/")
public class AgencyCreateTransferMoneyService {
    private static final String TAG = AgencyCreateTransferMoneyService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyCreateTransferMoney")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("childId") int childId,
                             @FormParam("money") double money) {
        AgencyCreateTransferMoneyResponse response = new AgencyCreateTransferMoneyResponse();
        response.childId = childId;
        response.money = money;
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else if (agency.appAgency.getParentId() > 0) {
            status = Status.PERMISSION_DENIED;
        } else if (money <= 0) {
            status = Status.MONEY_NOT_ZERO;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppAgency parentAppAgency = agency.appAgency;
                AppAgency childAppAgency = CacheManager.getAgencyById(session, childId);
                if (childAppAgency == null) {
                    status = Status.AGENCY_NOT_EXIST;
                } else if (childAppAgency.getParentId() != parentAppAgency.getId()) {
                    status = Status.PERMISSION_DENIED;
                } else {
                    AppTransferMoney appTransferMoney = new AppTransferMoney();
                    appTransferMoney.setParentId(parentAppAgency.getId());
                    appTransferMoney.setChildId(childId);
                    appTransferMoney.setMoney(money);
                    appTransferMoney.setState(Constant.TransferMoneyState.WAITING);
                    appTransferMoney.setCreated(new Timestamp(System.currentTimeMillis()));
                    session.save(appTransferMoney);

                    response.transferMoneyInfo = appTransferMoney.getTransferMoneyInfo(session);

                    //todo: tự tạo 1 khoản chi

                    AppNotify appNotify = new AppNotify();
                    appNotify.setOwnerId(childId);
                    appNotify.setParentId(agency.appAgency.getId());
                    appNotify.setChildId(childId);
                    appNotify.setTransferMoneyId(appTransferMoney.getId());
                    appNotify.setType(Constant.NotifyType.PARENT_TRANSFER_MONEY_TO_CHILD);
                    appNotify.setDes("Đại lý mẹ đã chuyển cho bạn " + Utils.formatNumber(money));
                    appNotify.setCreated(new Timestamp(System.currentTimeMillis()));
                    session.save(appNotify);

                    GcmUtils.sendNotification(session, appNotify.getOwnerId(),appNotify.getDes(), Utils.toString(appNotify));

                    status = Status.SUCCESSFULL;
                }

                transaction.commit();
            } catch (Exception e) {
                status = new Status(201, e.getMessage());
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
