package com.os.bbh.rest.dashboard.response;

import com.os.bbh.rest.ResponseBase;

import java.util.ArrayList;
import java.util.List;

public class AgencyGetDashBoard2DetailResponse extends ResponseBase {
    public int agencyId;
    public long fromDate;
    public long toDate;
    public List<DashBoardItem2> list = new ArrayList<>();
}
