package com.os.bbh.rest.voucher;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.*;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.voucher.response.AgencyCreateVoucherResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;

@Path("/")
public class AgencyCreateVoucherService {
    private static final String TAG = AgencyCreateVoucherService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyCreateVoucher")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("type") int type,
                             @FormParam("customerId") int customerId,
                             @FormParam("startDate") long startDate,
                             @FormParam("numDay") int numDay,
                             @FormParam("periodThuLai") int periodThuLai,
                             @FormParam("loan") double loan,
                             @FormParam("interestRateLai") double interestRateLai,
                             @FormParam("interestRateHo") float interestRateHo,
                             @FormParam("reference") String reference,
                             @FormParam("referencePhone") String referencePhone,
                             @FormParam("referenceDes") String referenceDes) {
        AgencyCreateVoucherResponse response = new AgencyCreateVoucherResponse();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else if (numDay <= 0) {
            status = Status.NUM_DAY_NOT_ZERO;
        } else if (loan <= 0) {
            status = Status.LOAN_NOT_ZERO;
        } else if (periodThuLai <= 0) {
            status = Status.PERIOD_THU_LAI_NOT_ZERO;
        } else if (type == Constant.VoucherType.LAI && interestRateLai <= 0) {
            status = Status.INTEREST_RATE_LAI_NOT_ZERO;
        } else if (type == Constant.VoucherType.HO && interestRateHo <= 0) {
            status = Status.INTEREST_RATE_HO_NOT_ZERO;
        } else if (reference != null && reference.length() > 0 && !Utils.isValidName(reference)) {
            status = Status.INVALID_NAME;
        } else if (referencePhone != null && referencePhone.length() > 0 && !Utils.isValidPhone(referencePhone)) {
            status = Status.INVALID_PHONE;
        } else if (referenceDes != null && referenceDes.length() > 0 && !Utils.isValidDes(referenceDes)) {
            status = Status.INVALID_DES;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                double moneyCustomerGet = 0;
                if (type == Constant.VoucherType.LAI) moneyCustomerGet = loan;
                else if (type == Constant.VoucherType.HO) moneyCustomerGet = loan * interestRateHo;

                AppAgency appAgency = agency.appAgency;
                AppCustomer appCustomer = CacheManager.getCustomer(session, customerId);
                if (appCustomer == null) {
                    status = Status.CUSTOMER_NOT_EXIST;
                } else if (!Utils.checkChildOrIsMe(session, appCustomer.getAgencyId(), agency.appAgency.getId())) {
                    status = Status.PERMISSION_DENIED;
                } else if (!appAgency.enough(moneyCustomerGet)) {
                    status = Status.NOT_ENOUGH_MONEY;
                } else {
                    AppVoucher appVoucher = new AppVoucher();
                    appVoucher.setType(type);
                    appVoucher.setAgencyId(agency.appAgency.getId());
                    appVoucher.setCustomerId(customerId);
                    if (startDate > 0) {
                        appVoucher.setStartDate(new Timestamp(startDate));
                        appVoucher.setEndDate(new Timestamp(startDate + Utils.dayToMillisecond(numDay)));
                    }
                    appVoucher.setNumDay(numDay);
                    appVoucher.setPeriodThuLai(periodThuLai);
                    appVoucher.setLoan(loan);
                    appVoucher.setRemainLoan(loan);
                    appVoucher.setInterestRateLai(interestRateLai);
                    appVoucher.setInterestRateHo(interestRateHo);
                    appVoucher.setReference(reference);
                    appVoucher.setReferencePhone(referencePhone);
                    appVoucher.setReferenceDes(referenceDes);
                    appVoucher.setState(Constant.VoucherState.RUNNING);
                    appVoucher.setCreated(new Timestamp(System.currentTimeMillis()));
                    session.save(appVoucher);

                    appCustomer.updateTotalDebt(loan);
                    appCustomer.updateRemainDebt(loan);
                    appCustomer.setState(Constant.CustomerState.ACTIVE);
                    session.update(appCustomer);

                    appAgency.updateMoney(-moneyCustomerGet);
                    session.update(appAgency);

                    AppStatistic appStatistic = new AppStatistic();
                    appStatistic.setAgencyId(appAgency.getId());
                    appStatistic.setMoney(moneyCustomerGet);
                    appStatistic.setType(Constant.StatisticType.CREATE_VOUCHER);
                    appStatistic.setAppVoucherId(appVoucher.getId());
                    session.save(appStatistic);

                    response.voucherInfo = appVoucher.getVoucherInfo();

                    status = Status.SUCCESSFULL;
                }

                transaction.commit();
            } catch (Exception e) {
                status = new Status(e);
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
