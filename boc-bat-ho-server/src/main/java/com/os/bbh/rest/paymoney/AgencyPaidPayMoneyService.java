package com.os.bbh.rest.paymoney;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppPayMoney;
import com.os.bbh.mysql.AppStatistic;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;
import java.util.List;

@Path("/")
public class AgencyPaidPayMoneyService {
    private static final String TAG = AgencyPaidPayMoneyService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyPaidPayMoney")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("payMoneyId") int payMoneyId) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                List<AppPayMoney> list = session.createQuery("from AppPayMoney where id=:payMoneyId and paid=0 and deleted=0 and agencyId=:agencyId")
                        .setParameter("payMoneyId", payMoneyId)
                        .setParameter("agencyId", agency.appAgency.getId())
                        .setMaxResults(1).list();
                if (list != null && list.size() > 0) {
                    AppPayMoney appPayMoney = list.get(0);

                    AppAgency appAgency = agency.appAgency;
                    if(appAgency.enough(appPayMoney.getMoney())){
                        appPayMoney.setPaid(1);
                        session.update(appPayMoney);

                        appAgency.updateMoney(-appPayMoney.getMoney());
                        session.update(appAgency);

                        AppStatistic appStatistic = new AppStatistic();
                        appStatistic.setAgencyId(appPayMoney.getAgencyId());
                        appStatistic.setMoney(appPayMoney.getMoney());
                        appStatistic.setType(Constant.StatisticType.AGENCY_PAID_PAY_MONEY);
                        appStatistic.setAppPayMoneyId(appPayMoney.getId());
                        session.save(appStatistic);

                        status = Status.SUCCESSFULL;
                    }else {
                        status = Status.NOT_ENOUGH_MONEY;
                    }
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
