package com.os.bbh.rest.dashboard;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.*;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.dashboard.response.AgencyGetDashBoard2DetailResponse;
import com.os.bbh.rest.dashboard.response.AgencyGetDashBoard2Response;
import com.os.bbh.rest.dashboard.response.DashBoardItem1;
import com.os.bbh.rest.dashboard.response.DashBoardItem2;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;
import java.util.List;

@Path("/")
public class AgencyGetDashBoard2DetailService {
    private static final String TAG = AgencyGetDashBoard2DetailService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyGetDashBoardDetail2")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("agencyId") int agencyId,
                             @FormParam("fromDate") long fromDate,
                             @FormParam("toDate") long toDate) {
        AgencyGetDashBoard2DetailResponse response = new AgencyGetDashBoard2DetailResponse();
        response.agencyId = agencyId;
        response.fromDate = fromDate;
        response.toDate = toDate;
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                List<AppStatistic> list = session.createQuery("from AppStatistic where agencyId=:agencyId and created>=:fromDate and created<=:toDate")
                        .setParameter("agencyId", agencyId)
                        .setParameter("fromDate", new Timestamp(fromDate))
                        .setParameter("toDate", new Timestamp(toDate))
                        .list();
                if (list != null && list.size() > 0) {
                    for (AppStatistic appStatistic : list) {
                        DashBoardItem2 item2 = new DashBoardItem2();
                        item2.agencyId = agencyId;
                        item2.type = appStatistic.getType();
                        item2.created = appStatistic.getCreated();
                        if (appStatistic.getType() == Constant.StatisticType.AGENCY_PAID_PAY_MONEY) {
                            List<AppPayMoney> list2 = session.createQuery("from AppPayMoney where id=:appPayMoneyId")
                                    .setParameter("appPayMoneyId", appStatistic.getAppPayMoneyId())
                                    .setMaxResults(1).list();
                            if (list2 != null && list2.size() > 0) {
                                AppPayMoney appPayMoney = list2.get(0);
                                item2.money = appPayMoney.getMoney();
                                item2.description = appPayMoney.getPurpose();
                                item2.payMoneyId = appPayMoney.getId();
                                response.list.add(item2);
                            }
                        } else if (appStatistic.getType() == Constant.StatisticType.COLLECT_DEBT) {
                            AppVoucher appVoucher = CacheManager.getVoucher(session, appStatistic.getAppVoucherId());
                            if (appVoucher != null) {
                                AppCustomer appCustomer = CacheManager.getCustomer(session, appVoucher.getCustomerId());
                                if (appCustomer != null) {
                                    item2.money = appStatistic.getMoney();
                                    item2.description = "Thu nợ " + appCustomer.getName();
                                    item2.voucherId = appVoucher.getId();
                                    response.list.add(item2);
                                }
                            }
                        } else if (appStatistic.getType() == Constant.StatisticType.CREATE_VOUCHER) {
                            AppVoucher appVoucher = CacheManager.getVoucher(session, appStatistic.getAppVoucherId());
                            if (appVoucher != null) {
                                AppCustomer appCustomer = CacheManager.getCustomer(session, appVoucher.getCustomerId());
                                if (appCustomer != null) {
                                    item2.money = appStatistic.getMoney();
                                    item2.description = "Tạo khoản vay " + appCustomer.getName();
                                    item2.voucherId = appVoucher.getId();
                                    response.list.add(item2);
                                }
                            }
                        }
                    }
                }

                status = Status.SUCCESSFULL;

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
