package com.os.bbh.rest.voucher.response;

import com.os.bbh.rest.ResponseBase;

import java.util.ArrayList;
import java.util.List;

public class AgencyGetHistoryVoucherResponse extends ResponseBase {
    public List<HistoryVoucherInfo> list = new ArrayList<>();
}
