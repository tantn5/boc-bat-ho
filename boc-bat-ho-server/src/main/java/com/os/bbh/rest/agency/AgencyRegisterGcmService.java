package com.os.bbh.rest.agency;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppRegisterGcm;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;
import java.util.List;

@Path("/")
public class AgencyRegisterGcmService {
    private static final String TAG = AgencyRegisterGcmService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyRegisterGcm")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("regId") String regId,
                             @FormParam("deviceId") String deviceId) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                List<AppRegisterGcm> list = session.createQuery("from AppRegisterGcm where deviceId=:deviceId")
                        .setParameter("deviceId", deviceId)
                        .setMaxResults(1)
                        .list();
                if (list != null && list.size() > 0) {
                    AppRegisterGcm appRegisterGcm = list.get(0);
                    appRegisterGcm.setAgencyId(agency.appAgency.getId());
                    appRegisterGcm.setRegId(regId);
                    session.update(appRegisterGcm);
                    status = Status.SUCCESSFULL;
                } else {
                    AppRegisterGcm appRegisterGcm = new AppRegisterGcm();
                    appRegisterGcm.setAgencyId(agency.appAgency.getId());
                    appRegisterGcm.setRegId(regId);
                    appRegisterGcm.setCreated(new Timestamp(System.currentTimeMillis()));
                    appRegisterGcm.setDeviceId(deviceId);
                    session.save(appRegisterGcm);
                    status = Status.SUCCESSFULL;
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
