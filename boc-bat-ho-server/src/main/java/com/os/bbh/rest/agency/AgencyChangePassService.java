package com.os.bbh.rest.agency;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;

@Path("/")
public class AgencyChangePassService {
    private static final String TAG = AgencyChangePassService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyChangePass")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("currentPass") String currentPass,
                             @FormParam("newPass") String newPass) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else if (!Utils.isValidPassword(newPass)) {
            status = Status.INVALID_PASSWORD;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppAgency appAgency = agency.appAgency;
                if (!appAgency.getPassword().equals(currentPass)) {
                    status = Status.INVALID_PASSWORD;
                } else {
                    appAgency.setPassword(newPass);
                    session.update(appAgency);
                    status = Status.SUCCESSFULL;
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
