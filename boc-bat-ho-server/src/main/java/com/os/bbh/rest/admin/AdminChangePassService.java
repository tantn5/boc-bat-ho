package com.os.bbh.rest.admin;

import com.os.bbh.app.AdminManager;
import com.os.bbh.app.model.Admin;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAdmin;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;

@Path("/")
public class AdminChangePassService {
    private static final String TAG = AdminChangePassService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AdminChangePass")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("currentPass") String currentPass,
                             @FormParam("newPass") String newPass) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;
        Admin admin = AdminManager.getAdminBySessionTicket(sessionTicket);
        if (admin == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else if (!Utils.isValidPassword(newPass)) {
            status = Status.INVALID_PASSWORD;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppAdmin appAdmin = admin.appAdmin;
                if (!appAdmin.getPassword().equals(currentPass)) {
                    status = Status.INVALID_PASSWORD;
                } else {
                    appAdmin.setPassword(newPass);
                    session.update(appAdmin);
                    status = Status.SUCCESSFULL;
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
