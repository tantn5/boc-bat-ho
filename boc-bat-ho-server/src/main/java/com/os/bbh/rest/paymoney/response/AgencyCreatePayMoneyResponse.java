package com.os.bbh.rest.paymoney.response;

import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.voucher.response.VoucherInfo;

public class AgencyCreatePayMoneyResponse extends ResponseBase {
    public PayMoneyInfo payMoneyInfo;
}
