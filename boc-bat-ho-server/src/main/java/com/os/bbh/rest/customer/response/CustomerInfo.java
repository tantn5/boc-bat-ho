package com.os.bbh.rest.customer.response;

import java.util.Date;
import java.util.List;

public class CustomerInfo {
    public int id;
    public int agencyId;
    public int createId;
    public String name;
    public String phone;
    public String address;
    public Date birthDay;
    public double totalDebt;
    public double remainDebt;
    public int state;
    public Date created;
    public String cmnd;
    public String hokhau;
    public List<ImageInfo> imageInfos;
}
