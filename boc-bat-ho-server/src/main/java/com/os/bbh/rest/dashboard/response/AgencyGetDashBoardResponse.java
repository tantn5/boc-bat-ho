package com.os.bbh.rest.dashboard.response;

import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.notify.response.NotifyInfo;

import java.util.ArrayList;
import java.util.List;

public class AgencyGetDashBoardResponse extends ResponseBase {
    public long fromDate;
    public long toDate;
    public double von;
    public double lai;
    public double thu;
    public double chi;
}
