package com.os.bbh.rest;

import com.os.bbh.common.Constant;

public class Status {
    public int code;
    public String msg;

    public Status(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Status(Exception e) {
        this.code = 201;
        this.msg = e.getMessage();
    }

    public static final Status SUCCESSFULL = new Status(200, "Thành công");
    public static final Status OPERATION_FAIL = new Status(201, "Có lỗi xảy ra. Vui lòng thử lại");
    public static final Status INVALID_ACCOUNT_OR_PASSWORD = new Status(202, "Tài khoản hoặc mật khẩu không hợp lệ");
    public static final Status ACCOUNT_EXIST = new Status(203, "Tài khoản đã tồn tại");
    public static final Status INVALID_USERNAME = new Status(204, "Tên tài khoản không hợp lệ, Tên tài khoản phải gồm các kí tự từ A-Z,a-z,0-9, có độ dài từ " + Constant.USERNAME_LENGTH_MIN + " đến " + Constant.USERNAME_LENGTH_MAX + " kí tự");
    public static final Status INVALID_PHONE = new Status(204, "Số điện thoại không hợp lệ");
    public static final Status INVALID_NAME = new Status(204, "Tên không hợp lệ, tên phải gồm các kí tự từ A-Z,a-z,0-9, có độ dài từ " + Constant.NAME_LENGTH_MIN + " đến " + Constant.NAME_LENGTH_MAX + " kí tự");
    public static final Status INVALID_PASSWORD = new Status(204, "Mật khẩu không hợp lệ, mật khẩu phải gồm các kí tự từ A-Z,a-z,0-9, có độ dài từ " + Constant.PASS_LENGTH_MIN + " đến " + Constant.PASS_LENGTH_MAX + " kí tự");
    public static final Status INVALID_DES = new Status(204, "Mô tả không hợp lệ, mô tả phải gồm các kí tự từ A-Z,a-z,0-9, có độ dài từ " + Constant.DES_LENGTH_MIN + " đến " + Constant.DES_LENGTH_MAX + " kí tự");
    public static final Status ACCOUNT_NOT_LOGIN = new Status(205, "Tài khoản chưa đăng nhập");
    public static final Status NOT_ENOUGH_MONEY = new Status(206, "Số dư không đủ");
    public static final Status PARENT_NOT_ENOUGH_MONEY = new Status(206, "Số dư đại lý mẹ không đủ");
    public static final Status CHILD_AGENCY_CANNOT_EDIT_VOUCHER = new Status(207, "Đại lý mẹ mới có quyền sửa chứng từ");
    public static final Status NUM_DAY_NOT_ZERO = new Status(208, "Số ngày phải lớn hơn 0");
    public static final Status LOAN_NOT_ZERO = new Status(208, "Khoản vay phải lớn hơn 0");
    public static final Status PERIOD_THU_LAI_NOT_ZERO = new Status(208, "Chu kì thu lãi phải lớn hơn 0");
    public static final Status INTEREST_RATE_LAI_NOT_ZERO = new Status(208, "Lãi suất phải lớn hơn 0");
    public static final Status INTEREST_RATE_HO_NOT_ZERO = new Status(208, "Lãi suất phải lớn hơn 0");
    public static final Status MONEY_NOT_ZERO = new Status(208, "Số tiền phải lớn hơn 0");
    public static final Status CUSTOMER_NOT_EXIST = new Status(209, "Khách hàng không tồn tại");
    public static final Status AGENCY_NOT_EXIST = new Status(209, "Đại lý không tồn tại");
    public static final Status KEY_NOT_EXIST = new Status(209, "Key không tồn tại");
    public static final Status KEY_USED= new Status(209, "Key đã sử dụng");
    public static final Status PERMISSION_DENIED = new Status(210, "Không được phép");
    public static final Status CANNOT_DELETE_BECAUSE_PAID = new Status(211, "Không được xoá khoản chi đã chi");
    public static final Status AGENCY_EXPIRED = new Status(212, "Đại lý hết hạn xử dụng. Vui lòng liên hệ với Admin để gia hạn");
}
