package com.os.bbh.rest.voucher;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppCustomer;
import com.os.bbh.mysql.AppVoucher;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.voucher.response.AgencyGetListVoucherResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.util.ArrayList;
import java.util.List;

@Path("/")
public class AgencyGetListVoucherService {
    private static final String TAG = AgencyGetListVoucherService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyGetListVoucher")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("agencyId") int agencyId,
                             @FormParam("customerId") int customerId,
                             @FormParam("fromPos") int fromPos,
                             @FormParam("pageSize") int pageSize) {
        AgencyGetListVoucherResponse response = new AgencyGetListVoucherResponse();
        Status status = Status.OPERATION_FAIL;
        if (pageSize == 0) pageSize = 10;
        response.fromPos = fromPos;
        response.pageSize = pageSize;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                if (agencyId > 0 && !Utils.checkChildOrIsMe(session, agencyId, agency.appAgency.getId())) {
                    status = Status.PERMISSION_DENIED;
                } else {
                    if (agencyId == 0) {
                        agencyId = agency.appAgency.getId();
                    }
                    List<Integer> listAgencyId = new ArrayList<>();
                    listAgencyId.add(agencyId);
                    listAgencyId.addAll(Utils.getListChildId(session, agencyId));
                    List<AppCustomer> customers = session.createQuery("from AppCustomer where (agencyId IN (?1) or createId IN (?1))")
                            .setParameterList("1", listAgencyId)
                            .setFirstResult(fromPos)
                            .setMaxResults(pageSize)
                            .list();

                    if (customers != null && customers.size() > 0) {
                        List<Integer> listCustomerId = new ArrayList<>();
                        for (AppCustomer appCustomer : customers) {
                            listCustomerId.add(appCustomer.getId());
                        }

                        List<AppVoucher> list = null;
                        if (customerId == 0) {
                            list = session.createQuery("from AppVoucher where customerId IN (?1)  and deleted=0")
                                    .setParameterList("1", listCustomerId)
                                    .setFirstResult(fromPos)
                                    .setMaxResults(pageSize)
                                    .list();
                        } else {
                            for (AppCustomer appCustomer : customers) {
                                if (customerId == appCustomer.getId()) {
                                    list = session.createQuery("from AppVoucher where customerId=:customerId and deleted=0")
                                            .setParameter("customerId", customerId)
                                            .setFirstResult(fromPos)
                                            .setMaxResults(pageSize)
                                            .list();
                                    break;
                                }
                            }
                        }
                        if (list != null) {
                            for (AppVoucher appVoucher : list) {
                                response.voucherInfos.add(appVoucher.getVoucherInfo());
                            }
                        }
                    }
                    status = Status.SUCCESSFULL;
                }

                transaction.commit();
            } catch (Exception e) {
                status = new Status(201, e.getMessage());
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
