package com.os.bbh.rest.transfermoney.response;

import com.os.bbh.rest.agency.response.AgencyInfo;

public class TransferMoneyInfo {
    public int transferMoneyId;
    public AgencyInfo parentInfo;
    public AgencyInfo childInfo;
    public double money;
    public int state;
    public long created;
}
