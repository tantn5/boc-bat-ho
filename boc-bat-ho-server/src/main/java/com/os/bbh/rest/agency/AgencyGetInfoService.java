package com.os.bbh.rest.agency;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.agency.response.AgencyAddChildResponse;
import com.os.bbh.rest.agency.response.AgencyGetInfoResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;

@Path("/")
public class AgencyGetInfoService {
    private static final String TAG = AgencyGetInfoService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyGetInfo")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("agencyId") int agencyId) {
        AgencyGetInfoResponse response = new AgencyGetInfoResponse();
        response.agencyId = agencyId;
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppAgency appAgency = CacheManager.getAgencyById(session, agencyId);
                if (appAgency != null) {
                    response.agencyInfo = appAgency.getAgencyInfo();
                    status = Status.SUCCESSFULL;
                } else {
                    status = Status.AGENCY_NOT_EXIST;
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
