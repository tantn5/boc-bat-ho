package com.os.bbh.rest.admin;

import com.os.bbh.app.AdminManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Admin;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAdmin;
import com.os.bbh.mysql.AppKey;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.admin.response.AdminCreateKeyResponse;
import com.os.bbh.rest.admin.response.AdminLoginResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;

@Path("/")
public class AdminCreateKeyService {
    private static final String TAG = AdminCreateKeyService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AdminCreateKey")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public AdminCreateKeyResponse post(@HeaderParam("sessionTicket") String sessionTicket,
                                       @FormParam("time") long time) {
        AdminCreateKeyResponse response = new AdminCreateKeyResponse();
        Status status = Status.OPERATION_FAIL;
        Admin admin = AdminManager.getAdminBySessionTicket(sessionTicket);
        if (admin == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppKey appKey = CacheManager.createKey(session, time, admin.appAdmin.getUsername());
                if (appKey != null) {
                    response.keyInfo = appKey.getKeyInfo();
                    status = Status.SUCCESSFULL;
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
