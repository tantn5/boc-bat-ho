package com.os.bbh.rest.agency.response;

import com.os.bbh.rest.ResponseBase;

public class AgencyLoginResponse extends ResponseBase {
    public String sessionTicket;
    public AgencyInfo agencyInfo;
}
