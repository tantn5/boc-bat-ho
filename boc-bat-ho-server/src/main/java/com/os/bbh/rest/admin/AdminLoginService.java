package com.os.bbh.rest.admin;

import com.os.bbh.app.AdminManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Admin;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAdmin;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.admin.response.AdminLoginResponse;
import com.os.bbh.rest.Status;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;

@Path("/")
public class AdminLoginService {
    private static final String TAG = AdminLoginService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AdminLogin")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public AdminLoginResponse post(@FormParam("username") String username,
                                   @FormParam("password") String password) {
        AdminLoginResponse response = new AdminLoginResponse();
        Status status = Status.OPERATION_FAIL;
        if (!Utils.isValidUsername(username)) {
            status = Status.INVALID_USERNAME;
        } else if (!Utils.isValidPassword(password)) {
            status = Status.INVALID_PASSWORD;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppAdmin appAdmin = CacheManager.getAdmin(session, username, password);
                if (appAdmin != null) {
                    String sessionId = Utils.randStr(16);
                    Admin admin = new Admin();
                    admin.sessionTicket = sessionId;
                    admin.appAdmin = appAdmin;
                    AdminManager.addAdmin(admin);

                    response.sessionTicket = sessionId;
                    response.adminInfo = appAdmin.getAdminInfo();
                    status = Status.SUCCESSFULL;
                } else {
                    status = Status.INVALID_ACCOUNT_OR_PASSWORD;
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
