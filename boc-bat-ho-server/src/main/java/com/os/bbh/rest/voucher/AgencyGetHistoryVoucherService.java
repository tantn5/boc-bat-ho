package com.os.bbh.rest.voucher;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.GcmUtils;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.*;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.voucher.response.AgencyActionVoucherResponse;
import com.os.bbh.rest.voucher.response.AgencyGetHistoryVoucherResponse;
import com.os.bbh.rest.voucher.response.HistoryVoucherInfo;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;
import java.util.List;

@Path("/")
public class AgencyGetHistoryVoucherService {
    private static final String TAG = AgencyGetHistoryVoucherService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyGetHistoryVoucher")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("voucherId") int voucherId) {
        AgencyGetHistoryVoucherResponse response = new AgencyGetHistoryVoucherResponse();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                List<AppStatistic> list = session.createQuery("from AppStatistic where appVoucherId=:voucherId and type=:type")
                        .setParameter("voucherId", voucherId)
                        .setParameter("type", Constant.StatisticType.COLLECT_DEBT)
                        .list();
                if (list != null && list.size() > 0) {
                    for (AppStatistic appStatistic : list) {
                        HistoryVoucherInfo historyVoucherInfo = new HistoryVoucherInfo();
                        historyVoucherInfo.action = HistoryVoucherInfo.HistoryVoucherAction.COLLECT_DEBT;
                        historyVoucherInfo.money = appStatistic.getMoney();
                        historyVoucherInfo.date = appStatistic.getCreated().getTime();
                        response.list.add(historyVoucherInfo);
                    }
                }
                status = Status.SUCCESSFULL;
                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }

}
