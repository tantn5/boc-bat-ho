package com.os.bbh.rest.dashboard;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppStatistic;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.dashboard.response.AgencyGetDashBoard2Response;
import com.os.bbh.rest.dashboard.response.AgencyGetDashBoardResponse;
import com.os.bbh.rest.dashboard.response.DashBoardItem1;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;
import java.util.List;

@Path("/")
public class AgencyGetDashBoard2Service {
    private static final String TAG = AgencyGetDashBoard2Service.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyGetDashBoard2")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("fromDate") long fromDate,
                             @FormParam("toDate") long toDate) {
        AgencyGetDashBoard2Response response = new AgencyGetDashBoard2Response();
        response.fromDate = fromDate;
        response.toDate = toDate;
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                List<AppAgency> listAgency = session.createQuery("from AppAgency where parentId=:parentId")
                        .setParameter("parentId", agency.appAgency.getId())
                        .list();
                if (listAgency != null) {
                    for (AppAgency appAgency : listAgency) {
                        DashBoardItem1 item1 = new DashBoardItem1();
                        item1.agencyId = appAgency.getId();
                        item1.agencyName = appAgency.getName();
                        List<AppStatistic> list = session.createQuery("from AppStatistic where agencyId=:agencyId and created>=:fromDate and created<=:toDate")
                                .setParameter("agencyId", appAgency.getId())
                                .setParameter("fromDate", new Timestamp(fromDate))
                                .setParameter("toDate", new Timestamp(toDate))
                                .list();
                        if (list != null && list.size() > 0) {
                            for (AppStatistic appStatistic : list) {
                                if (appStatistic.getType() == Constant.StatisticType.AGENCY_PAID_PAY_MONEY) {
                                    item1.chi += appStatistic.getMoney();
                                } else if (appStatistic.getType() == Constant.StatisticType.COLLECT_DEBT) {
                                    item1.thu += appStatistic.getMoney();
                                } else if (appStatistic.getType() == Constant.StatisticType.CREATE_VOUCHER) {
                                    item1.chi += appStatistic.getMoney();
                                }
                            }
                        }
                        response.list.add(item1);
                    }
                }

                status = Status.SUCCESSFULL;

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
