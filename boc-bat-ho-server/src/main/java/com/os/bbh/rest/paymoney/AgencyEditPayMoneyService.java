package com.os.bbh.rest.paymoney;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppPayMoney;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;
import java.util.List;

@Path("/")
public class AgencyEditPayMoneyService {
    private static final String TAG = AgencyEditPayMoneyService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyEditPayMoney")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("payMoneyId") int payMoneyId,
                             @FormParam("money") double money,
                             @FormParam("purpose") String purpose,
                             @FormParam("payDate") long payDate,
                             @FormParam("type") int type) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                List<AppPayMoney> list = session.createQuery("from AppPayMoney where id=:payMoneyId and paid=0 and deleted=0 and agencyId=:agencyId")
                        .setParameter("payMoneyId", payMoneyId)
                        .setParameter("agencyId", agency.appAgency.getId())
                        .setMaxResults(1).list();
                if (list != null && list.size() > 0) {
                    AppPayMoney appPayMoney = list.get(0);
                    appPayMoney.setAgencyId(agency.appAgency.getId());
                    appPayMoney.setMoney(money);
                    appPayMoney.setPurpose(purpose);
                    appPayMoney.setType(type);
                    appPayMoney.setPayDate(new Timestamp(payDate));
                    session.update(appPayMoney);

                    status = Status.SUCCESSFULL;
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
