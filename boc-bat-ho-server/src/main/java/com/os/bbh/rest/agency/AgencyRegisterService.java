package com.os.bbh.rest.agency;

import com.os.bbh.app.CacheManager;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;

@Path("/")
public class AgencyRegisterService {
    private static final String TAG = AgencyRegisterService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyRegister")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@FormParam("username") String username,
                             @FormParam("phone") String phone,
                             @FormParam("name") String name,
                             @FormParam("address") String address,
                             @FormParam("password") String password) {
        ResponseBase response = new ResponseBase();
        Status status = Status.OPERATION_FAIL;
        if (name == null) name = "AGC " + Utils.randInt(10000, 99999);
//        if (!Utils.isValidPhone(phone)) {
//            status = Status.INVALID_PHONE;
        //} else if (!Utils.isValidName(name)) {
            //status = Status.INVALID_NAME;
        //} else
            if (!Utils.isValidUsername(username)) {
            status = Status.INVALID_USERNAME;
        } else if (!Utils.isValidPassword(password)) {
            status = Status.INVALID_PASSWORD;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                AppAgency appAgency = CacheManager.getAgency(session, username);
                if (appAgency != null) {
                    status = Status.ACCOUNT_EXIST;
                } else {
                    appAgency = CacheManager.createAgency(session, username, password, phone, name, address, 0);
                    if (appAgency == null) {
                        status = Status.OPERATION_FAIL;
                    } else {
                        status = Status.SUCCESSFULL;
                    }
                }

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
