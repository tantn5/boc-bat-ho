//package com.os.bbh.rest.common;
//
//import com.os.bbh.common.Utils;
//import org.apache.commons.io.IOUtils;
//import org.apache.commons.lang.time.DateUtils;
//import org.jboss.resteasy.plugins.providers.multipart.InputPart;
//import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
//
//import javax.imageio.ImageIO;
//import javax.ws.rs.*;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.MultivaluedMap;
//import javax.ws.rs.core.Response;
//import java.awt.image.BufferedImage;
//import java.io.*;
//import java.text.ParseException;
//import java.util.Date;
//import java.util.List;
//import java.util.Locale;
//import java.util.Map;
//
//@Path("/")
//public class ViewImageService {
//    private static final String TAG = ViewImageService.class.getSimpleName();
//
//    private final String UPLOADED_FILE_PATH = "d:\\";
//
////    @GET
////    //@Consumes(MediaType.MULTIPART_FORM_DATA)
////    @Produces("image/png")
////    @Path("/Viewimage/{imageName}")
////    public File viewImage() {
////        return new File("C:\\Users\\Nickgun\\Desktop\\512x512_A.png");
////    }
//
//    @GET
//    //@Consumes(MediaType.MULTIPART_FORM_DATA)
//    @Produces("image/png")
//    @Path("/Viewimage/{imgName}")
//    public Response viewImage(@PathParam("imgName") String imgName) {
//
//        // set file (and path) to be download
//        //File file = new File("/home/uploadfiledata/"+imgName);
//        File file = new File("C:\\Users\\Nickgun\\Desktop\\coin-anim.png");
//        Response.ResponseBuilder responseBuilder = Response.ok((Object) file);
//        responseBuilder.header("Content-Disposition", "attachment; filename=\"" + imgName + "\"");
//        return responseBuilder.build();
//    }
//}
package com.os.bbh.rest.common;

import com.os.bbh.common.Utils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.time.DateUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.imageio.ImageIO;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Path("/")
public class ViewImageService {
    private static final String TAG = ViewImageService.class.getSimpleName();
//    @GET
//    //@Consumes(MediaType.MULTIPART_FORM_DATA)
//    @Produces("image/png")
//    @Path("/Viewimage/{imageName}")
//    public File viewImage() {
//        return new File("C:\\Users\\Nickgun\\Desktop\\512x512_A.png");
//    }

    @GET
    //@Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces("image/png")
    @Path("/ViewImage/{imgName}")
    public Response viewImage(@PathParam("imgName") String imgName) throws Exception {
        BufferedImage image = ImageIO.read(new File("/home/uploadfiledata/"+imgName));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", baos);
        byte[] imageData = baos.toByteArray();
        return Response.ok(new ByteArrayInputStream(imageData)).build();

//        // set file (and path) to be download
//        File file = new File("/home/uploadfiledata/"+imgName);
//        //File file = new File("C:\\Users\\Nickgun\\Desktop\\coin-anim.png");
//        Response.ResponseBuilder responseBuilder = Response.ok((Object) file);
//        responseBuilder.header("Content-Disposition", "attachment; filename=\"" + imgName + "\"");
//        return responseBuilder.build();
    }

//    @GET
//    //@Consumes(MediaType.MULTIPART_FORM_DATA)
//    @Produces("image/png")
//    @Path("/Viewimage")
//    public File viewImage() {
//        return new File("C:\\Users\\Nickgun\\Desktop\\512x512_A.png");
//    }
}
