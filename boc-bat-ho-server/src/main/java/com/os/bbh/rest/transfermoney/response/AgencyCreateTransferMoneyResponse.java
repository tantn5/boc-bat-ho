package com.os.bbh.rest.transfermoney.response;

import com.os.bbh.rest.ResponseBase;

import java.util.ArrayList;
import java.util.List;

public class AgencyCreateTransferMoneyResponse extends ResponseBase {
    public int childId;
    public double money;
    public TransferMoneyInfo transferMoneyInfo;
}
