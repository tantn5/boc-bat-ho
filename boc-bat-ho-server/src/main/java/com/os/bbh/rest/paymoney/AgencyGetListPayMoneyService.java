package com.os.bbh.rest.paymoney;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppPayMoney;
import com.os.bbh.mysql.AppVoucher;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.paymoney.response.AgencyGetListPayMoneyResponse;
import com.os.bbh.rest.voucher.response.AgencyGetListVoucherResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.sql.Timestamp;
import java.util.List;

@Path("/")
public class AgencyGetListPayMoneyService {
    private static final String TAG = AgencyGetListPayMoneyService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyGetListPayMoney")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("fromDate") long fromDate,
                             @FormParam("toDate") long toDate,
                             @FormParam("fromPos") int fromPos,
                             @FormParam("pageSize") int pageSize) {
        AgencyGetListPayMoneyResponse response = new AgencyGetListPayMoneyResponse();
        Status status = Status.OPERATION_FAIL;
        if (pageSize == 0) pageSize = 10;
        response.fromPos = fromPos;
        response.pageSize = pageSize;
        response.fromDate = fromDate;
        response.toDate = toDate;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                List<AppPayMoney> list = session.createQuery("from AppPayMoney where agencyId=:agencyId and deleted=0 and created>=:fromDate and created<=:toDate")
                        .setParameter("agencyId", agency.appAgency.getId())
                        .setParameter("fromDate", new Timestamp(fromDate))
                        .setParameter("toDate", new Timestamp(toDate))
                        .setFirstResult(fromPos)
                        .setMaxResults(pageSize)
                        .list();
                if (list != null) {
                    for (AppPayMoney appPayMoney : list) {
                        response.payMoneyInfos.add(appPayMoney.getPayMoneyInfo());
                    }
                }
                status = Status.SUCCESSFULL;

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
