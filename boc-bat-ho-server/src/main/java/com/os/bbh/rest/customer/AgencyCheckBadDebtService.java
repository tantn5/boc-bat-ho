package com.os.bbh.rest.customer;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.CacheManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppCustomer;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.customer.response.AgencyCheckBadDebtResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("/")
public class AgencyCheckBadDebtService {
    private static final String TAG = AgencyCheckBadDebtService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencyCheckBadDebt")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public AgencyCheckBadDebtResponse post(@HeaderParam("sessionTicket") String sessionTicket,
                                           @FormParam("cmnd") String cmnd,
                                           @FormParam("phone") String phone,
                                           @FormParam("hokhau") String hokhau) {
        AgencyCheckBadDebtResponse response = new AgencyCheckBadDebtResponse();
        Status status = Status.OPERATION_FAIL;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                double remainDebt = 0;
                boolean badDebt = false;
                Map<Integer, AppCustomer> map = new HashMap<>();
                if (phone != null && phone.length() > 0) phone = Utils.formatPhone("84", phone);
                if (phone != null && phone.length() > 0) {
                    List<AppCustomer> list = session.createQuery("from AppCustomer where phone=:phone")
                            .setParameter("phone", phone).list();
                    if (list != null && list.size() > 0) {
                        for (AppCustomer appCustomer : list) map.put(appCustomer.getId(), appCustomer);
                    }
                }
                if (!badDebt) {
                    if (cmnd != null && cmnd.length() > 0) {
                        List<AppCustomer> list = session.createQuery("from AppCustomer where cmnd=:cmnd")
                                .setParameter("cmnd", cmnd).list();
                        if (list != null && list.size() > 0) {
                            for (AppCustomer appCustomer : list) map.put(appCustomer.getId(), appCustomer);
                        }
                    }
                }
                if (!badDebt) {
                    if (hokhau != null && hokhau.length() > 0) {
                        List<AppCustomer> list = session.createQuery("from AppCustomer where hokhau=:hokhau")
                                .setParameter("hokhau", hokhau).list();
                        if (list != null && list.size() > 0) {
                            for (AppCustomer appCustomer : list) map.put(appCustomer.getId(), appCustomer);
                        }
                    }
                }

                response.isOldCustomer = map.size() > 0;
                for (AppCustomer appCustomer : map.values()) {
                    if (appCustomer.getState() == Constant.CustomerState.PAUSE) {
                        badDebt = true;
                        remainDebt += appCustomer.getRemainDebt();
                        response.customerInfos.add(appCustomer.getCustomerInfo(session));
                    }
                }

                status = Status.SUCCESSFULL;

                response.remainDebt = remainDebt;
                response.isBadDebt = badDebt;
                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }
}
