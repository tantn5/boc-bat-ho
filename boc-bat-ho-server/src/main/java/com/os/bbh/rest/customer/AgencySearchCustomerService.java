package com.os.bbh.rest.customer;

import com.os.bbh.app.AgencyManager;
import com.os.bbh.app.model.Agency;
import com.os.bbh.common.Constant;
import com.os.bbh.common.Utils;
import com.os.bbh.mysql.AppAgency;
import com.os.bbh.mysql.AppCustomer;
import com.os.bbh.mysql.HibernateUtils;
import com.os.bbh.rest.ResponseBase;
import com.os.bbh.rest.Status;
import com.os.bbh.rest.customer.response.AgencySearchCustomerResponse;
import com.os.bbh.rest.customer.response.CustomerInfo;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import java.util.ArrayList;
import java.util.List;

@Path("/")
public class AgencySearchCustomerService {
    private static final String TAG = AgencySearchCustomerService.class.getSimpleName();

    @POST // This annotation indicates GET request
    @Path("/AgencySearchCustomer")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    public ResponseBase post(@HeaderParam("sessionTicket") String sessionTicket,
                             @FormParam("text") String text,
                             @FormParam("state") int state,
                             @FormParam("fromPos") int fromPos,
                             @FormParam("pageSize") int pageSize) {
        AgencySearchCustomerResponse response = new AgencySearchCustomerResponse();
        Status status = Status.OPERATION_FAIL;
        if (pageSize == 0) pageSize = 10;
        response.text = text;
        response.fromPos = fromPos;
        response.pageSize = pageSize;
        Agency agency = AgencyManager.getAgencyBySessionTicket(sessionTicket);
        if (agency == null) {
            status = Status.ACCOUNT_NOT_LOGIN;
        } else {
            Session session = HibernateUtils.getSession();//open session
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();

                List<AppCustomer> list = null;
                List<Integer> listAgencyId = new ArrayList<>();
                listAgencyId.add(agency.appAgency.getId());
                //get list child
                List<AppAgency> listChild = session.createQuery("from AppAgency where parentId=:parentId")
                        .setParameter("parentId", agency.appAgency.getId()).list();
                if (listChild != null && listChild.size() > 0) {
                    for (AppAgency appAgency : listChild) {
                        listAgencyId.add(appAgency.getId());
                    }
                }
                if (state != Constant.CustomerState.ALL && text != null && text.length() > 0) {
                    list = session.createQuery("from AppCustomer where createId IN (?1) and state=:state and (phone like :textsearch or nameAscii like :textsearch)")
                            .setParameterList("1", listAgencyId)
                            .setParameter("textsearch", "%" + text + "%")
                            .setParameter("state", state)
                            .setFirstResult(fromPos)
                            .setMaxResults(pageSize)
                            .list();
                } else if (state != Constant.CustomerState.ALL && (text == null || text.length() == 0)) {
                    list = session.createQuery("from AppCustomer where createId IN (?1) and state=:state")
                            .setParameterList("1", listAgencyId)
                            .setParameter("state", state)
                            .setFirstResult(fromPos)
                            .setMaxResults(pageSize)
                            .list();
                } else if (state == Constant.CustomerState.ALL && text != null && text.length() > 0) {
                    list = session.createQuery("from AppCustomer where createId IN (?1) and (phone like :textsearch or nameAscii like :textsearch)")
                            .setParameterList("1", listAgencyId)
                            .setParameter("textsearch", "%" + text + "%")
                            .setFirstResult(fromPos)
                            .setMaxResults(pageSize)
                            .list();
                } else {
                    list = session.createQuery("from AppCustomer where createId IN (?1)")
                            .setParameterList("1", listAgencyId)
                            .setFirstResult(fromPos)
                            .setMaxResults(pageSize)
                            .list();
                }
                if (list != null) {
                    for (AppCustomer appCustomer : list) {
                        response.customerInfos.add(appCustomer.getCustomerInfo(session));
                    }
                }
                status = Status.SUCCESSFULL;

                transaction.commit();
            } catch (Exception e) {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                Utils.error(TAG, e);
            }
        }
        response.setStatus(status);
        return response;
    }

    public static void main(String[] args) {
        Session session = HibernateUtils.getSession();//open session
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            List<Integer> listAgencyId = new ArrayList<>();
            listAgencyId.add(1);
            listAgencyId.add(2);
            listAgencyId.add(3);
            String text = "";
            int state = Constant.CustomerState.ALL;
            int fromPos = 0;
            int pageSize = 10;
            List<AppCustomer> list = session.createQuery("from AppCustomer where createId IN (?1) and state=:state and (phone like :textsearch or nameAscii like :textsearch)")
                    .setParameterList("1", listAgencyId)
                    .setParameter("textsearch", "%" + text + "%")
                    .setParameter("state", state)
                    .setFirstResult(fromPos)
                    .setMaxResults(pageSize)
                    .list();
            List<CustomerInfo> customerInfos = new ArrayList<>();
            for (AppCustomer appCustomer : list) {
                customerInfos.add(appCustomer.getCustomerInfo(session));
                ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
                String json = ow.writeValueAsString(appCustomer);
                System.out.println(json);
            }
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}
